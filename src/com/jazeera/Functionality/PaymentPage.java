package com.jazeera.Functionality;

import java.io.IOException;
import com.framework.commonReusableClasses.*;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;

public class PaymentPage extends ByXpath {
	private WebElement iframe;	
	
	By creditCardTabEle = By.xpath(".//a[@id='ExternalAccount_tab']");
	By pagePaymentEle = By.xpath(".//div[@class='payment-area paymentPart']");
	By savedCardEle = By.xpath(".//*[@id='SaveCreditCard']/div/div/label/input[@id='creditCard_0']/following-sibling::span[@class='radio-label']");
	By payAnotherCardEle = By.xpath("//*[@id='frmMultiCreditCard']//a[@class='back-to-single-credit-card']/span");
	By masterCardNumberEle = By.xpath(".//input[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_MC_ACCTNO']");
	By visaCardNumberEle = By.xpath(".//input[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_VI_ACCTNO']");
	By masterMonthEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_MC_EXPDAT_MONTH']");
	By masterYearEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_MC_EXPDAT_YEAR']");
	By masterCvvEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_MC_CC::VerificationCode']");
	By masterFnameCustEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_MC_CC::AccountHolderName']");
	By visaMonthEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_VI_EXPDAT_MONTH']");
	By visaYearEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_VI_EXPDAT_YEAR']");
	By visaCvvEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_VI_CC::VerificationCode']");
	By visaFnameCustEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_ControlGroupPaymentInputViewPaymentView_ExternalAccount_VI_CC::AccountHolderName']");
	By visaDrpDwnEle = By.xpath("//*[@id='ExternalAccount_PaymentMethodCode']/option[2]");
	By iframeEle = By.xpath("//*[@id='authWindow']");
	By secureCodeEle = By.xpath("//*[@name='external.field.password']");	
	By submitBtnEle = By.xpath(".//input[@value='Submit']");
	By stateEle = By.xpath(".//*[@id='credit-card-StateTextBox0']");
	By knetPayNowBtnEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_LinkButtonSubmit']");
	By knetTabEle = By.xpath("//*[@id='PrePaid_tab']");
	By knetPageEle = By.xpath(".//*[@id='form1']");
	By knetDropdownEle = By.xpath(".//*[@id='bank']");
	By knetCardNumberEle = By.xpath(".//*[@id='cardN']");
	By knetMonthEle = By.xpath(".//*[@name='Ecom_Payment_Card_ExpDate_Month']");
	By knetPinEle = By.xpath(".//*[@id='Ecom_Payment_Pin_id']");
	By knetSubmitBtnEle = By.xpath(".//*[@id='EntrySubmitAction_id']");
	By knetConfirmationBtnEle = By.xpath(".//*[@id='ConfirmAction_id']");
	By agencyPayNowBtnEle = By.xpath("//*[@id='CONTROLGROUPPAYMENTBOTTOM_LinkButtonSubmit']/em");
	By mexCountryStateNameEle = By.xpath(".//*[@id='credit-card-StateDropDown0']");	
	By cityEle = By.xpath("//*[@id='js-citycode0']");
	By zipCodeEle = By.xpath("//*[@id='js-zipcode0']");
	By emailEle = By.xpath("//*[@id='js-email0']");
	By phoneNoEle = By.xpath("//*[@id='js-phone0']");
	By checkAcceptPaymenyEle = By.xpath("//*[@id='screen-reader-termsconditions']");
	By iframeVisaCheckEle = By.xpath("//div[@data-formid='frmVisaWalletApplication']//iframe[contains(@src, 'https://www.google.com/recaptcha/api2/')]");
	By captchaOnPaymentEle = By.xpath("//*[@id='recaptcha-anchor-label']");
	By payMyTripEle = By.xpath(".//a[@id='CONTROLGROUPPAYMENTBOTTOM_LinkButtonSubmit']");
	By payInCashEle = By.xpath("//a[@data-payment-type='thirdparty']");
	By drpdwnPayInCashEle = By.xpath("//*[@id='payment-div-third-party']//div[1]//div[2]//div[1]//button//span[1]");
	By drpdwnPayInCashOptionEle = By.xpath("//*[@id='payment-div-third-party']//div[1]//div[2]//div[1]//ul//li[1]//a");
	By captchaPayInCashEle = By.xpath("//div[@data-formid='frmthirdparty']//iframe[contains(@src, 'https://www.google.com/recaptcha/api2/')]");
	By checkBtnPayInCashEle = By.xpath("//*[@id='third-party-payment-terms-checkbox']");
	By electronicCreditEle = By.xpath("//a[@data-payment-type='voucher']");
	By electronicCreditDivEle = By.xpath("//*[@id='payment-div-voucher']");
	By voucherEle = By.xpath("//*[@id='voucher-code-fullInput']");
	By activateVoucherEle = By.xpath("//*[@id='confirmVoucher']");
	By electronicInvoiceTabEle = By.xpath("//a[@data-formid='frmCreditCard']");
	By cardVoucherEle = By.xpath("//*[@id='voucher-js-credit-card-numberId0']");
	By monthVoucherEle = By.xpath("//*[@id='voucher-cc-month0']");
	By yearVoucherEle = By.xpath("//*[@id='voucher-cc-year0']");
	By cvvVoucherEle = By.xpath("//*[@id='voucher-cvv-code0']");
	By fNameVoucherEle = By.xpath("//*[@id='voucher-cc-firstname0']");
	By lNameVoucherEle = By.xpath("//*[@id='voucher-cc-lastname0']");
	By streetVoucherEle = By.xpath("//*[@id='voucher-address0']");	
	By savedCardVClubEle = By.xpath("//input[@id='creditCard_0']/following-sibling::span");
	By cvvSavedCardVClubEle = By.xpath("//*[@id='saved-payment-cvv-0']");	
	By payPalEle = By.xpath("//a[@data-payment-type='paypalpreparepayment']");
	By buyPayPalEle = By.xpath("//*[@id='payment-div-paypal']//button[@type='submit']");
	By captchaPayPalEle = By.xpath("//iframe[contains(@src, 'https://www.sandbox.paypal.com/signin/')]");
	By usernamePayPalEle = By.xpath("//*[@id='email']");
	By pwdPayPalEle = By.xpath("//*[@id='password']");
	By submitPayPalEle = By.xpath("//*[@id='btnLogin']");
	By payNowPayPalEle = By.xpath("//*[@id='confirmButtonTop']");	
	By agencyPaymentEle = By.xpath("//a[@data-payment-type='agency']");
	By cardAgencyEle = By.xpath("//*[@id='agency-js-credit-card-numberId0']");
	By monthAgencyEle = By.xpath("//*[@id='agency-cc-month0']");
	By yearAgencyEle = By.xpath("//*[@id='agency-cc-year0']");
	By cvvAgencyEle = By.xpath("//*[@id='agency-cvv-code0']");
	By fNameAgencyEle = By.xpath("//*[@id='agency-cc-firstname0']");
	By lNameAgencyEle = By.xpath("//*[@id='agency-cc-lastname0']");
	By streetAgencyEle = By.xpath("//*[@id='agency-address0']");	
	By visaCheckoutEle = By.xpath("//a[@data-payment-type='visawallet']");
	By visaChckoutImgEle = By.xpath("//img[@class='v-button'][@role='button']");
	By visaChckoutIFrameEle = By.xpath("//*[@id='VMECheckoutIframe']");
	By usernameVisaChckoutEle = By.xpath(".//*[@id='user_name']");
	By username1VisaChckoutEle = By.xpath("//*[contains(@id, 'user')]");	
	By continueBtnVisaChckEle = By.xpath("//*[@id='main']//input[@class='viewButton-button'][@type='submit']");
	By pwdVisaChckEle = By.xpath(".//*[@id='password']");	
	By signInToVisaChckEle = By.xpath("//*[@id='login']//input[@type='submit'][@data-reactid='.0.1.0.4.0.0:$signin.$signin.0.2.0.1.0.0']");	
	By continue2BtnVisachckEle = By.xpath("//input[@data-reactid='.0.1.0.4.0.0:$review.$review.4.1.0.0.0']");
	By noThanksVisaChckEle = By.xpath("//a[@aria-label='Continue without setting'][@data-reactid='.0.1.0.4.0.0:$ssioptin.1.3.1.0']");
	By cvvVisaChckEle = By.xpath("//div[not(contains(@class, 'visa-cvv hidden'))]/div/input[@id='js-cvvCode']");
	By masterPassEle = By.xpath("//a[@data-payment-type='mastercardwallet']");
	By popupConfrmtionPageEle = By.xpath("//*[@id='poup-close']");
	By itineraryPageEle = By.xpath(".//div[@class='bookingconfirmationContent']");
	By pnrNumberEle = By.xpath("//*/div[@class='bookingconfirmationContent']/div[1]/span[2]");
	By iframeContainer = By.id("dynamic-popup");
	By selectPaymentListEle = By.xpath(".//*[@class='dropdown dropdown_payment']");
	By selectVisaListEle = By.xpath(".//*[@id='payment-div-credit-card']//*[@class='dropdown-menu js-dropdown-paymentType']/li[1]/a");
	By loaderPaypalEle = By.xpath("//*[@class='loader']");
	By checkoutLoaderEle = By.xpath("//div[contains(@class,'vmeCheckoutSpinner')]");

	
    public PaymentPage(RemoteWebDriver driver) {
        super(driver);
        dynamicWait = new DynamicWait(driver);
        jexecutor = new Jexecutor(driver);
    }

    public void paymentType(String type) throws IOException, InterruptedException {
    	dynamicWait.checkPageLoaderInvisiblity();
    	dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, pagePaymentEle);  	
		log1.info("Selecting Payment type and filling info.");
		switch(type.toLowerCase()) {
		case "visa":
			click(creditCardTabEle);
			click(visaDrpDwnEle);
			fillCardInformation("visaCard");
			break;
		case "mastercard":
			click(creditCardTabEle);
			fillCardInformation("masterCard");
			break;
		case "declinemastercard":
			click(creditCardTabEle);
			fillCardInformation("declineMasterCard");
			break;
		case "knet":
			click(knetTabEle);
			knetPayment("knet");
			break;
		case "declineknet":
			click(knetTabEle);
			knetPayment("declineknet");
			break;
		case "agencycredit":
			click(agencyPayNowBtnEle);
			break;
		case "invexzero":
			
			break;
		case "invextwo":
			paymentInvexTwo();
			break;
		case "paypal":
			paymentPayPal();
			break;
		case "vclubsavedcard":
			paymentVClubSavedCard();
			break;
		case "payincash":
			paymentPayIncash();
			break;
		case "visacheckout":
			paymentVisaCheckout();
			break;
		case "masterpass":
			paymentMasterPass();
			break;
		case "voucher":
			paymentElectronicVoucher();
			break;
		case "agencysplit":
			paymentAgencySplit();
			break;
		case "agency":
			paymentAgency();
			break;
		}
	}
    
    public void listAndSavedCard() throws InterruptedException{   
    	dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, pagePaymentEle);
    	if(isObjectAvailable(selectPaymentListEle, 2)) {
            click(selectPaymentListEle);
            click(selectVisaListEle);
		} else if(isObjectAvailable(savedCardEle, 2))
			click(payAnotherCardEle);
		else
			System.out.println("");
    }
	
	private void knetPayment(String cardType) throws IOException, InterruptedException {
		click(knetPayNowBtnEle);
		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, knetPageEle);
		drpdwnByText(knetDropdownEle, ReadPropertyFile.read("knetBank"));
		//click(knetDropdownEle);
		if (cardType == "knet") {
			sendKeys(knetCardNumberEle, ReadPropertyFile.read("knetCard")); }
		else
			sendKeys(knetCardNumberEle, ReadPropertyFile.read("declineKnetCard"));
		drpdwnByText(knetMonthEle, ReadPropertyFile.read("knetMonth"));		
		sendKeys(knetPinEle, ReadPropertyFile.read("knetPin"));
		click(knetSubmitBtnEle);
		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, knetConfirmationBtnEle);
		click(knetConfirmationBtnEle);
		if (cardType == "declineknet") {
			driver.navigate().back();
			driver.navigate().back();
			drpdwnByText(knetDropdownEle, ReadPropertyFile.read("knetBank"));
			sendKeys(knetCardNumberEle, ReadPropertyFile.read("knetCard"));
			drpdwnByText(knetMonthEle, ReadPropertyFile.read("knetMonth"));		
			sendKeys(knetPinEle, ReadPropertyFile.read("knetPin"));
			click(knetSubmitBtnEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, knetConfirmationBtnEle);
			click(knetConfirmationBtnEle);
			}
		/*sendKeys(fnameCustEle, ReadPropertyFile.read("firstName"));
		sendKeys(lnameCustEle, ReadPropertyFile.read("lastName"));
		sendKeys(addressEle, ReadPropertyFile.read("address"));
		sendKeys(cityEle, ReadPropertyFile.read("cityName"));
		sendKeys(zipCodeEle, ReadPropertyFile.read("zip"));
		sendKeys(emailEle, ReadPropertyFile.read("customerEmail"));
		sendKeys(phoneNoEle, ReadPropertyFile.read("phoneNo"));
		jClick(checkAcceptPaymenyEle);
		captchaClick(captchaOnPaymentEle);
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
		jClick(payMyTripEle);*/		
	}
	
	private void paymentInvexTwo() throws IOException, InterruptedException {
		/*dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, cardNumberEle);
		sendKeys(cardNumberEle, ReadPropertyFile.read("invexTwoCard"));
		drpdwnByText(monthEle, ReadPropertyFile.read("invexTwoExpMonth"));
		drpdwnByText(yearEle, ReadPropertyFile.read("invexTwoExpYear"));
		sendKeys(cvvEle, ReadPropertyFile.read("invexTwoCvv"));
		sendKeys(fnameCustEle, ReadPropertyFile.read("firstName"));
		sendKeys(lnameCustEle, ReadPropertyFile.read("lastName"));
		sendKeys(addressEle, ReadPropertyFile.read("address"));
		sendKeys(cityEle, ReadPropertyFile.read("cityName"));
		sendKeys(zipCodeEle, ReadPropertyFile.read("zip"));
		sendKeys(emailEle, ReadPropertyFile.read("customerEmail"));
		sendKeys(phoneNoEle, ReadPropertyFile.read("phoneNo"));
		jClick(checkAcceptPaymenyEle);
		captchaClick(captchaOnPaymentEle);
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
		jClick(payMyTripEle);*/	
	}
	
	private void paymentPayPal() throws InterruptedException {
        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payPalEle);
		if (isObjectAvailable(payPalEle)) {
			jClick(payPalEle);
			jClick(buyPayPalEle);
			sendKeys(usernamePayPalEle, ReadPropertyFile.read("usernamePayPal"));
			sendKeys(pwdPayPalEle, ReadPropertyFile.read("pwdPayPal"));
			click(submitPayPalEle);
            dynamicWait.checkPageLoaderInvisiblity(loaderPaypalEle);
            dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payNowPayPalEle);
			jClick(payNowPayPalEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}
	
	private void paymentVClubSavedCard() throws InterruptedException {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, savedCardEle);
		if (isObjectAvailable(payPalEle)) {
			jClick(savedCardEle);
			sendKeys(savedCardVClubEle, ReadPropertyFile.read("cvvNo"));
			jClick(checkAcceptPaymenyEle);
			log1.info("Accepting payment");
			captchaClick(captchaOnPaymentEle);
			log1.info("Switching captcha.");
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}
	
	private void paymentPayIncash() throws IOException, InterruptedException {
        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payInCashEle);
		if (isObjectAvailable(payInCashEle)) {
			jClick(payInCashEle);			
			if(isObjectAvailable(drpdwnPayInCashEle, 5)) {
				jClick(drpdwnPayInCashEle);
				jClick(drpdwnPayInCashOptionEle);
			}
			jClick(checkBtnPayInCashEle);
			iframeElementClick(captchaPayInCashEle, captchaOnPaymentEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}
	
	private void paymentVisaCheckout() throws InterruptedException {
		if (isObjectAvailable(visaCheckoutEle, 10)) {
			jClick(visaCheckoutEle);
			dynamicWait.checkPageLoaderInvisiblity();
            Thread.sleep(1000);
			jClick(visaChckoutImgEle);
			dynamicWait.checkPageLoaderInvisiblity(checkoutLoaderEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, visaChckoutIFrameEle);
			iframe = getElement(visaChckoutIFrameEle);
			switchToFrame(iframe);
//			click(usernameVisaChckoutEle);		
			Thread.sleep(1000);
//			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, usernameVisaChckoutEle);
			sendKeys(usernameVisaChckoutEle, ReadPropertyFile.read("usernameVisaChckout"));
			click(continueBtnVisaChckEle);
			click(pwdVisaChckEle);
			sendKeys(pwdVisaChckEle, ReadPropertyFile.read("pwdVisaChckoutEle"));
			click(signInToVisaChckEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE,continue2BtnVisachckEle);
			click(continue2BtnVisachckEle);
			if(isObjectAvailable(noThanksVisaChckEle, 5)) {
			    click(noThanksVisaChckEle);
			} else
			    System.out.println("");
			switchToDefaultContent();
			sendKeys(cvvVisaChckEle, ReadPropertyFile.read("cvvNo"));
			jClick(checkAcceptPaymenyEle);
			iframeElementClick(iframeVisaCheckEle, captchaOnPaymentEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);
  		} else
  			throw new NoSuchElementException("Element is not found.");
	 }	
	
	private void paymentMasterPass() throws InterruptedException {
        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, masterPassEle);
		if (isObjectAvailable(masterPassEle)) {
			jClick(masterPassEle);
			jClick(visaChckoutImgEle);
			iframeElementSendData(visaChckoutIFrameEle, usernameVisaChckoutEle, ReadPropertyFile.read("usernameVisaChckout"));
			iframeElementClick(visaChckoutIFrameEle, continueBtnVisaChckEle);
			iframeElementSendData(visaChckoutIFrameEle, pwdVisaChckEle, ReadPropertyFile.read("usernameVisaChckout"));
			iframeElementClick(visaChckoutIFrameEle, signInToVisaChckEle);
			iframeElementClick(visaChckoutIFrameEle, continueBtnVisaChckEle);
			sendKeys(cvvVisaChckEle, ReadPropertyFile.read("cvvNo"));
			jClick(checkAcceptPaymenyEle);
			captchaClick(captchaOnPaymentEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}
	
	private void paymentElectronicVoucher() throws IOException, InterruptedException {
        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, electronicCreditEle);
		if (isObjectAvailable(electronicCreditEle)) {
			jClick(electronicCreditEle);
			sendKeys(voucherEle, ReadPropertyFile.read("voucher"));
			getElement(By.xpath("//*[@id='voucher-code-fullInput']")).sendKeys(Keys.TAB);
			jClick(activateVoucherEle);
			try {
				if(isObjectAvailable(cardVoucherEle)){}
			} catch (NoSuchElementException ex) {
				jClick(electronicInvoiceTabEle);
			}
			sendKeys(cardVoucherEle, ReadPropertyFile.read("visaCard"));
			drpdwnByText(monthVoucherEle, ReadPropertyFile.read("expMonth"));
			drpdwnByText(yearVoucherEle, ReadPropertyFile.read("expYear"));
			sendKeys(cvvVoucherEle, ReadPropertyFile.read("cvvNo"));
			sendKeys(fNameVoucherEle, ReadPropertyFile.read("firstName"));
			sendKeys(lNameVoucherEle, ReadPropertyFile.read("lastName"));
			sendKeys(streetVoucherEle, ReadPropertyFile.read("address"));
			sendKeys(cityEle, ReadPropertyFile.read("cityName"));
			sendKeys(zipCodeEle, ReadPropertyFile.read("zip"));
			sendKeys(emailEle, ReadPropertyFile.read("customerEmail"));
			sendKeys(phoneNoEle, ReadPropertyFile.read("phoneNo"));
			jClick(checkAcceptPaymenyEle);
			captchaClick(captchaOnPaymentEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}
	
	private void paymentAgency() throws InterruptedException {
        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, agencyPaymentEle);
		if (isObjectAvailable(agencyPaymentEle)) {
			jClick(agencyPaymentEle);
			jClick(checkAcceptPaymenyEle);
			captchaClick(captchaOnPaymentEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}
	
	private void paymentAgencySplit() throws InterruptedException {
        dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, agencyPaymentEle);
		if (isObjectAvailable(agencyPaymentEle)) {
			jClick(agencyPaymentEle);
			sendKeys(cardAgencyEle, ReadPropertyFile.read("visaCard"));
			drpdwnByText(monthAgencyEle, ReadPropertyFile.read("expMonth"));
			drpdwnByText(yearAgencyEle, ReadPropertyFile.read("expYear"));
			sendKeys(cvvAgencyEle, ReadPropertyFile.read("cvvNo"));
			sendKeys(fNameAgencyEle, ReadPropertyFile.read("firstName"));
			sendKeys(lNameAgencyEle, ReadPropertyFile.read("lastName"));
			sendKeys(streetAgencyEle, ReadPropertyFile.read("cityName"));
			sendKeys(cityEle, ReadPropertyFile.read("cityName"));
			sendKeys(zipCodeEle, ReadPropertyFile.read("zip"));
			sendKeys(emailEle, ReadPropertyFile.read("customerEmail"));
			sendKeys(phoneNoEle, ReadPropertyFile.read("phoneNo"));				
			click(checkAcceptPaymenyEle);
			captchaClick(captchaOnPaymentEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
			jClick(payMyTripEle);			
		} else
			throw new NoSuchElementException("Element is not found.");
	}

	private void captchaClick(By by) throws InterruptedException {
		iframe =  getElement(By.xpath("//div[@data-formid='']//iframe[contains(@src, 'https://www.google.com/recaptcha/api2/')]"));
		switchToFrame(iframe);
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, by);
		click(by);
//		Thread.sleep(3000);
		switchToDefaultContent();
	}

	private void iframeElementClick(By iFramXpath, By by) throws InterruptedException {
		iframe = getElement(iFramXpath);
		switchToFrame(iframe);
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, by);
		jClick(by);
		switchToDefaultContent();
//		Thread.sleep(1000);
	}

	private  void iframeElementSendData(By iFramXpath, By xpath, String data) throws InterruptedException {
		iframe = getElement(iFramXpath);
		switchToFrame(iframe);
		Thread.sleep(1000); 
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, xpath);
		WebElement element = getElement(xpath);
		element.sendKeys(data);
		click(submitBtnEle);
		switchToDefaultContent();
		Thread.sleep(2000);
	}

	public String confirmationBooking() throws InterruptedException {
//        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, iframeContainer);
//        sendEsc();
//		jClick(popupConfrmtionPageEle);
		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, itineraryPageEle);
		log1.info("Payment Done Successful.");        
        return getText(pnrNumberEle);
	}

	private void fillCardInformation(String paymentType) throws InterruptedException {
		
		if(paymentType == "visaCard")
		{
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, visaCardNumberEle);
			sendKeys(visaCardNumberEle, ReadPropertyFile.read(paymentType));
			drpdwnByText(visaMonthEle, ReadPropertyFile.read("expMonth"));
	        drpdwnByText(visaYearEle, ReadPropertyFile.read("expYear"));
	        sendKeys(visaCvvEle, ReadPropertyFile.read("cvvNo"));
	        sendKeys(visaFnameCustEle, ReadPropertyFile.read("firstName"));
	        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
	        click(payMyTripEle);
		}
        
		if (paymentType == "masterCard")
		{
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, masterCardNumberEle);
			sendKeys(masterCardNumberEle, ReadPropertyFile.read(paymentType));
	        drpdwnByText(masterMonthEle, ReadPropertyFile.read("expMonth"));
	        drpdwnByText(masterYearEle, ReadPropertyFile.read("expYear"));
	        sendKeys(masterCvvEle, ReadPropertyFile.read("cvvNo"));
	        sendKeys(masterFnameCustEle, ReadPropertyFile.read("firstName"));
	        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
	        click(payMyTripEle);
	        iframeElementSendData(iframeEle, secureCodeEle, ReadPropertyFile.read("secureCode"));
		}
		
		if (paymentType == "declineMasterCard")
		{
			/*---Decline Payment--*/
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, masterCardNumberEle);
			sendKeys(masterCardNumberEle, ReadPropertyFile.read(paymentType));
	        drpdwnByText(masterMonthEle, ReadPropertyFile.read("expMonth"));
	        drpdwnByText(masterYearEle, ReadPropertyFile.read("expYear"));
	        sendKeys(masterCvvEle, ReadPropertyFile.read("cvvNo"));
	        sendKeys(masterFnameCustEle, ReadPropertyFile.read("firstName"));
	        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
	        click(payMyTripEle);
	        
	        /*---Correct Payment--*/
	        
	        click(creditCardTabEle);
	        sendKeys(masterCardNumberEle, ReadPropertyFile.read("masterCard"));
	        drpdwnByText(masterMonthEle, ReadPropertyFile.read("expMonth"));
	        drpdwnByText(masterYearEle, ReadPropertyFile.read("expYear"));
	        sendKeys(masterCvvEle, ReadPropertyFile.read("cvvNo"));
	        sendKeys(masterFnameCustEle, ReadPropertyFile.read("firstName"));
	        dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, payMyTripEle);
	        click(payMyTripEle);
	        iframeElementSendData(iframeEle, secureCodeEle, ReadPropertyFile.read("secureCode"));        
	        
		}
        //sendKeys(secureCodeEle, ReadPropertyFile.read("secureCode"));
        /*sendKeys(lnameCustEle, ReadPropertyFile.read("lastName"));
        sendKeys(addressEle, ReadPropertyFile.read("address"));        
        drpdwnByValue(mexCountryNameEle, ReadPropertyFile.read("mexCountryName"));
        //drpdwnByValue(mexCountryStateNameEle, ReadPropertyFile.read("mexCountryStateName"));        
        sendKeys(cityEle, ReadPropertyFile.read("cityName"));
        sendKeys(zipCodeEle, ReadPropertyFile.read("zip"));
        sendKeys(emailEle, ReadPropertyFile.read("customerEmail"));
        sendKeys(phoneNoEle, ReadPropertyFile.read("phoneNo"));
        jexecutor.scrollToEnd();        
        if(className.equals("ChromeEmulator"))
        	checkBtnPayment();
        else
        	jClick(checkAcceptPaymenyEle);
        captchaClick(captchaOnPaymentEle);*/
    }
	
	public void checkBtnPayment() throws InterruptedException {
		driver.findElement(By.xpath(".//*[@id='screen-reader-termsconditions']/a[1]")).sendKeys(Keys.SHIFT, Keys.TAB);
		Thread.sleep(500);
		sendSpace();		
	}
}
