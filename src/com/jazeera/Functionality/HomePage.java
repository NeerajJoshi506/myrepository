package com.jazeera.Functionality;

import com.framework.commonReusableClasses.*;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HomePage extends ByXpath {
	
	By homeEle = By.xpath(".//*[@id='booking-widget']");
	By originEle = By.xpath("//*[@id='ControlGroupSearchView_AvailabilitySearchInputSearchVieworiginStationText1'][@placeholder='From']");
	By originListEle = By.xpath(".//*[@id='station_list'][@class='suggestions_holder']");
	By destinationEle = By.xpath("//*[@id='ControlGroupSearchView_AvailabilitySearchInputSearchViewdestinationStationText1'][@placeholder='To']");
	By destinationListEle = By.xpath(".//*[@id='station_list2']");
	By usaTabEle = By.xpath(".//*[@id='liCity1']/a");
	By dXBPopUpEle = By.xpath("//*[@id='DXBGCCPopUp']");
	By whileTravellingPopUpEle = By.xpath(".//*[@id='WhileTravelingAlertModelDiv']");
	By internationalCarryDocAlert = By.xpath("//*[@id='internationalCarryDocAlert']");
	By internationalCarryDocPopUpEle = By.xpath(".//*[@id='internationalCarryDocAlertModelDiv']");
	By impMsgSelectedPopUpEle = By.xpath(".//*[@id='ImportantMessageSelectedAlertModelDiv']");
	By impMsgSuspendedPopUpEle = By.xpath(".//*[@id='ImportantMessageSuspendedAlertModelDiv']");
	By creditCardPolicyPopUpEle = By.xpath(".//*[@id='CreditCardPolicyAlertModelDiv']");
	By kindlyAlertPopUpEle = By.xpath(".//*[@id='KindlyAlertModelDiv']");
	By totalPassengersPopUpEle = By.xpath(".//*[@id='TotalPassengersModelDiv']");
	By internationalLawsPopupAlertEle = By.xpath(".//*[@id='internationalLawsPopupAlert']");
	By oneWayRadioBtnEle = By.xpath(".//input[@id='ControlGroupSearchView_AvailabilitySearchInputSearchView_OneWay'][@value='OneWay']");
	By datePickerFromEle = By.xpath(".//*[@id='ControlGroupSearchView_AvailabilitySearchInputSearchView_TextBoxMarketDate1']");
	By datePickerToEle = By.xpath(".//*[@id='ControlGroupSearchView_AvailabilitySearchInputSearchView_TextBoxMarketDate2']");
	By adultEle = By.xpath(".//*[@id='txtAdultValue' and @data-maximum='9']");
	By minorEle = By.xpath(".//*[@id='txtMinorValue' and @data-maximum='9']");
	By infantsEle = By.xpath(".//*[@id='txtInfantValue' and @data-maximum='9']");
	By searchFlightBtnEle = By.xpath(".//td[@class='updatePart']/a");
	By adultDropdownEle = By.xpath(".//select[@id='ControlGroupSearchView_AvailabilitySearchInputSearchView_DropDownListPassengerType_ADT']");
	By childDropdownEle = By.xpath(".//select[@id='ControlGroupSearchView_AvailabilitySearchInputSearchView_DropDownListPassengerType_CHD']");
	By infantDropdownEle = By.xpath(".//select[@id='ControlGroupSearchView_AvailabilitySearchInputSearchView_DropDownListPassengerType_INFANT']");
	By datePickerEle = By.xpath("//*[@id='ui-datepicker-div']//span[@class='ui-icon ui-icon-circle-triangle-e']");
	By languageLnkEle = By.xpath("//*[@id='LangSelector']/span");
	By languageDrpdwnEle = By.xpath(".//*[@id='bs-country']//*[@id='language-popover']/button");
	By languageMXNEle = By.xpath(".//*[@id='nav-right']//a[@data-value='es-MX']//span[contains(text(), 'MEX')]");
	By languageUSEle = By.xpath(".//*[@id='nav-right']//a[@data-value='en-US']//span[contains(text(), 'USA')]");
	By currencyDrpdwnEle = By.xpath(".//*[@id='bs-country']//*[@id='country-currency']/div/button");	
	By currencyUSDELe = By.xpath(".//*[@class='LanguageShow dropdown v-dropdown open']//span[text()='Dollar (USD)']");
	By currencyMXNDEle = By.xpath(".//*[@class='LanguageShow dropdown v-dropdown open']//a[@data-currency='MXN']/span[text()='Peso (MXN)']");
	By currencyCRCEle = By.xpath(".//*[@class='LanguageShow dropdown v-dropdown open']//a[@data-currency='CRC']/span[text()='Colon (CRC)']");
	By currencyGTQEle = By.xpath(".//*[@class='LanguageShow dropdown v-dropdown open']//a[@data-currency='GTQ']/span[text()='Quetzal (GTQ)']");
	By currencyNIOEle = By.xpath(".//*[@class='LanguageShow dropdown v-dropdown open']//a[@data-currency='NIO']/span[text()='Cordoba (NIO']");
	By flightWidgetEle = By.xpath(".//*[@id='widget-tabs']//a[@href='#flightwidget']/span[2]");
	By homeImgEle = By.xpath("//img[contains(@src,'https://uat-cms.volaris.com/link') and @class='first-slide']");
    By image = By.xpath("//*[@class='first-slide']");
	By searchFlgtHeaderEle = By.xpath(".//*[@id='BookingWidgetFlight']/h3");

	Jexecutor jexecutor;

	
	public HomePage(RemoteWebDriver driver) {
		super(driver);
		jexecutor = new Jexecutor(driver);
		dynamicWait = new DynamicWait(driver);
	}

	public String testCaseDescription(String origin, String destination, String journeyType, String departureDate, String arrivalDate, String adult, String child, String infant) {
		if(journeyType.equalsIgnoreCase("oneway")) {
			return journeyType + "-" + origin + " to " + destination + " for" + departureDate + " with " + adult + " Adult " + child + " Child" + infant + " Infant";
		} else {
			return journeyType + "-" + origin + " to " + destination + " from" + departureDate + " to " + arrivalDate + " with " + adult + " Adult " + child + " Child" + infant + " Infant";
		}
	}

	private By getSourceLocator(String cityName) {
		String cityloc = String.format("//*[@id='station_list']/ul/li/p[@id='source_%s']", cityName.toUpperCase());
		return By.xpath(cityloc);
	}
	
	private By getDestinationLocator(String cityName) {
		String cityloc = String.format("//*[@id='station_list2']/ul/li/p[@id='dest_%s']", cityName.toUpperCase());
		return By.xpath(cityloc);
	}

	private void selectFlightStations(String originStationCode, String destinationStationCode) throws InterruptedException {
		Thread.sleep(500);
		click(originEle);
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE,originListEle);
		Thread.sleep(500);
		//click(originListEle);
		if (isObjectAvailable(getSourceLocator(originStationCode), 1)) {
			click(getSourceLocator(originStationCode));}		
		Thread.sleep(500);
		click(destinationEle);
		Thread.sleep(500);
		click(destinationListEle);
		if (isObjectAvailable(getDestinationLocator(destinationStationCode), 1)) {
			click(getDestinationLocator(destinationStationCode)); }
		if(isObjectAvailable(dXBPopUpEle,1)|| isObjectAvailable(internationalLawsPopupAlertEle,1)|| isObjectAvailable(whileTravellingPopUpEle,1)|| isObjectAvailable(internationalCarryDocPopUpEle,1)|| isObjectAvailable(impMsgSelectedPopUpEle,1)|| isObjectAvailable(impMsgSuspendedPopUpEle,1)|| isObjectAvailable(creditCardPolicyPopUpEle,1)|| isObjectAvailable(kindlyAlertPopUpEle,1) || isObjectAvailable(totalPassengersPopUpEle,1)|| isObjectAvailable(internationalCarryDocAlert,1)) {
			sendEsc();}			
	}

	public void searchFlights(String origin, String destination, String journeyType, String departureDate, String arrivalDate, String adult, String child, String infant) throws InterruptedException, ParseException {
		log1.info("Searching journey type.");
		selectFlightStations(origin, destination);
		selectFlightDates(journeyType, departureDate, arrivalDate);		
		selectPax(adult, child, infant);
		clickSearch();
	}

	private void selectFlightDates(String journeyType, String departureDate, String arrivalDate) throws InterruptedException, ParseException {
		click(datePickerFromEle);
		click(selectDate(departureDate));
		if(journeyType.equalsIgnoreCase("oneway")) {
			jClick(oneWayRadioBtnEle);
		}else {
			click(datePickerToEle);
			//click(selectDate(arrivalDate));
			click(selectReturnDateOnCal(departureDate, arrivalDate));
		}
	}

	private void selectPax(String adult, String minor, String infant) throws InterruptedException {
		//click(paxDropdownEle);
		/*jexecutor.setAttributeValue(adultEle, "value", adult);
		jexecutor.setAttributeValue(minorEle, "value", minor);
		jexecutor.setAttributeValue(infantsEle, "value", infant);*/
		click(adultDropdownEle);
		drpdwnByValue(adultDropdownEle, adult);
		click(childDropdownEle);
		drpdwnByValue(childDropdownEle, minor);
		click(infantDropdownEle);
		drpdwnByValue(infantDropdownEle, infant);
	}

	public void clickSearch() throws InterruptedException {
		windowHandle1();
		jClick(searchFlightBtnEle);
		windowHandle();
	}

	public By selectDate(String departureDate) throws InterruptedException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar calendar = Calendar.getInstance();
		int currentMonth = calendar.get(Calendar.MONTH);
		String currDate = simpleDateFormat.format(calendar.getTime());
		int dateIncrement = 0;
		try {
			dateIncrement = (int) (0 + (Math.abs(simpleDateFormat.parse(departureDate).getTime() - simpleDateFormat.parse(currDate).getTime()) / 86400000));
		} catch (ParseException ex) {
			Assert.fail("Error selecting dates");
			ex.printStackTrace();
			dateIncrement = 0;
		}
		calendar.add(Calendar.DATE, dateIncrement );
		int currentDate = calendar.get(Calendar.DATE);
		calendar.add(Calendar.MONTH, 0);
		int nextMonth = calendar.get(Calendar.MONTH);
		int monthDifference = nextMonth - currentMonth;
		if(monthDifference < 0) {
			monthDifference = monthDifference + 12;
		}
		for (int i = 0; i < monthDifference ; i++) {
			Thread.sleep(1000);
			getElement(datePickerEle).click();
		}
		//String dateFormat = String.format("//*[@id='ui-datepicker-div']/table/tbody/tr/td/a[@class='ui-state-default'][text()='%s']", String.valueOf(currentDate));
		String dateFormat = String.format("//*[contains(@onclick,'%s')]/a[text()='%s']",String.valueOf(nextMonth), String.valueOf(currentDate));
		return By.xpath(dateFormat);
	}

	public By selectReturnDateOnCal(String departureDate, String returnDate) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		//String currDate = departureDate;
		//Calendar calendar = Calendar.getInstance();
		Date date1 = simpleDateFormat.parse(departureDate);
		Date date2 = simpleDateFormat.parse(returnDate);
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		calendar1.setTime(date2);
		int retMonth = calendar1.get(Calendar.MONTH);
		int retDate = calendar1.get(Calendar.DATE);		
		calendar1.setTime(date1);
		calendar2.setTime(date2);
		int diffYear = Math.abs(calendar1.get(Calendar.YEAR) - calendar2.get(Calendar.YEAR));
		int diffMonth = diffYear * 12 + Math.abs(calendar1.get(Calendar.MONTH) - calendar2.get(Calendar.MONTH));
		for (int i = 0; i < diffMonth ; i++) {
			getElement(datePickerEle).click();
		}        
		//String dateFormat = String.format("//td//a[text()='%s']", returnDate.substring(0, 1).equals("0")?returnDate.substring(1, 2):returnDate.substring(0, 2));
		//String dateFormat = String.format("//*[@id='ui-datepicker-div']/table/tbody/tr/td/a[@class='ui-state-default'][text()='%s']", String.valueOf(returnDate));
		//String dateFormat = String.format("//a[text()='%s']/parent::td[@data-month='%s']/a",returnDate.substring(0, 2),calendar2.get(Calendar.MONTH));
		String dateFormat = String.format("//*[contains(@onclick,'%s')]/a[text()='%s']",String.valueOf(retMonth), String.valueOf(retDate));
		

		return By.xpath(dateFormat);
	}

	public By selectReturnDate(String date) {
		String dateFormat = String.format("//td//a[text()='%s']", date.substring(0, 2));
		String month = getElement(By.xpath(".//td[@class='ui-datepicker-today']")).getAttribute("data-month");
		String calYear = getElement(By.xpath(dateFormat)).getAttribute("data-year");
		String monthD = date.substring(3, date.length()-5).trim();
		String yearD = date.substring(date.length()-4).trim();
		while(!(monthD.equals(month) && yearD.equals(calYear)))	{
			getElement(datePickerEle).click();
			month = getElement(By.xpath("//*[@class='ui-datepicker-month']")).getText();
			calYear = getElement(By.xpath("//*[@class='ui-datepicker-year']")).getText();
		}
		return By.xpath(dateFormat);
	}

	private void selectFlightStationsMob(String originStationCode, String destinationStationCode) throws InterruptedException {
		/*click(originEle);
		Thread.sleep(500);
		jClick(originListEle);
		if (isObjectAvailable(getCityLocator(originStationCode), 1)) {
		} else {
			click(usaTabEle);
			if(isObjectAvailable(getCityLocator(originStationCode), 1)) {
			} else
				click(centralAmericaTabEle);
		}
		click(getCityLocator(originStationCode));
		jClick(destinationEle);
		sendKeys(destinationEle, destinationStationCode);
		Thread.sleep(500);
		click(destinationListEle);
		if (isObjectAvailable(getCityLocator(destinationStationCode), 1)) {
		} else {
			click(usaTabEle);
			if(isObjectAvailable(getCityLocator(destinationStationCode), 1)) {
			} else
				click(centralAmericaTabEle);
		}
		//click(getCityLocator(destinationStationCode));
		getElement(searchFlgtHeaderEle).click();
	*/}

	private void selectFlightDatesMob(String journeyType, String departureDate, String arrivalDate) throws InterruptedException, ParseException {
		click(datePickerFromEle);
		actionClick(selectDateMob(departureDate));
		if(journeyType.equalsIgnoreCase("oneway")) {
			jClick(oneWayRadioBtnEle);
		}else {
//			click(datePickerToEle);
			Thread.sleep(1000);
			jClick(selectReturnDateOnCalMob(departureDate, arrivalDate));
		}
	}

	private void selectPaxMob(String adult, String minor, String infant) throws InterruptedException {
		//click(paxDropdownEle);
		jexecutor.scrollToEnd();
		jexecutor.setAttributeValue(adultEle, "value", adult);
		jexecutor.setAttributeValue(minorEle, "value", minor);
		clickDownUntilVisible(infantsEle, minorEle);
		jexecutor.setAttributeValue(infantsEle, "value", infant);
	}

	public void searchFlightsMob(String origin, String destination, String journeyType, String departureDate, String arrivalDate, String adult, String child, String infant) throws InterruptedException, ParseException {
		log1.info("Searching journey type.");
		click(flightWidgetEle);
		selectFlightStationsMob(origin, destination);
		selectFlightDatesMob(journeyType, departureDate, arrivalDate);
		selectPaxMob(adult, child, infant);
		clickSearch();
	}

	public By selectDateMob(String departureDate) throws InterruptedException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar calendar = Calendar.getInstance();
		int currentMonth = calendar.get(Calendar.MONTH);
		String currDate = simpleDateFormat.format(calendar.getTime());
		int dateIncrement = 0;
		try {
			dateIncrement = (int) (0 + (Math.abs(simpleDateFormat.parse(departureDate).getTime() - simpleDateFormat.parse(currDate).getTime()) / 86400000));
		} catch (ParseException ex) {
			Assert.fail("Error selecting dates");
			ex.printStackTrace();
			dateIncrement = 0;
		}
		calendar.add(Calendar.DATE, dateIncrement );
		int currentDate = calendar.get(Calendar.DATE);
		calendar.add(Calendar.MONTH, 0);
		int nextMonth = calendar.get(Calendar.MONTH);
		int monthDifference = nextMonth - currentMonth;
		if(monthDifference < 0) {
			monthDifference = monthDifference + 12;
		}
		for (int i = 0; i < monthDifference ; i++) {
			getElement(datePickerEle).click();
		}
		String dateFormat = String.format("//td[@data-month='%s']//a[text()='%s']", String.valueOf(nextMonth), String.valueOf(currentDate));
		clickDownUntilVisible(By.xpath(dateFormat), destinationEle);
		return By.xpath(dateFormat);
	}

	public By selectReturnDateOnCalMob(String departureDate, String returnDate) throws ParseException, InterruptedException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		//String currentDate = departureDate;
		Date date1 = simpleDateFormat.parse(departureDate);
		Date date2 = simpleDateFormat.parse(returnDate);
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		calendar1.setTime(date1);
		calendar2.setTime(date2);
		int diffYear = Math.abs(calendar1.get(Calendar.YEAR) - calendar2.get(Calendar.YEAR));
		int diffMonth = diffYear * 12 + Math.abs(calendar1.get(Calendar.MONTH) - calendar2.get(Calendar.MONTH));
		for (int i = 0; i < diffMonth ; i++) {
			getElement(datePickerEle).click();
		}
		String dateFormat = String.format("//td//a[text()='%s']", returnDate.substring(0, 2));
		return By.xpath(dateFormat);
	}

	public By selectReturnDateMob(String date) throws InterruptedException {
		String dateFormat = String.format("//td//a[text()='%s']", date.substring(0, 2));
		String month = getElement(By.xpath(".//td[@class='ui-datepicker-today']")).getAttribute("data-month");
		String calYear = getElement(By.xpath(dateFormat)).getAttribute("data-year");
		String monthD = date.substring(3, date.length()-5).trim();
		String yearD = date.substring(date.length()-4).trim();
		while(!(monthD.equals(month) && yearD.equals(calYear)))	{
			getElement(datePickerEle).click();
			month = getElement(By.xpath("//*[@class='ui-datepicker-month']")).getText();
			calYear = getElement(By.xpath("//*[@class='ui-datepicker-year']")).getText();
		}
		clickDownUntilVisible(By.xpath(dateFormat), destinationEle);
		return By.xpath(dateFormat);
	}

	public void selectCultureAndCurrency(String cul, String currency) throws InterruptedException {
		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, image, 60);
		String culture = driver.getCurrentUrl();
		switch (cul.toUpperCase()) {
		case "EN":
			culture = culture + "?culture=en-US";
			break;
		case "ES":
			culture = culture + "?culture=es-MX";
			break;
		default:
		}
		switch (currency.toUpperCase()) {
		case "MXN":
			culture = culture + "&currency=MXN";
			break;
		case "USD":
			culture = culture + "&currency=USD";
			break;
		default:
		}
		driver.navigate().to(culture);
		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, image);
	}

	public enum  Environment {
        UAT, 
        PROD, 
    }

	public String goToUrl(Environment url) {
        switch (url) {
        case UAT:
            return "http://uat-beta.volaris.com/";
        case PROD:
            return  "http://www.volaris.com/";
        default:
            return "http://uat-beta.volaris.com/";
       }
    }
	
	public void selectCulture(String culture) throws InterruptedException {
		jClick(languageLnkEle);		
		jClick(languageDrpdwnEle);
		switch (culture) {		
		case "EN":
			jClick(languageUSEle);
			break;
		case "ES":
			jClick(languageMXNEle);
			break;
		}
	}

	public void selectCurrency(String Currency) throws InterruptedException {
		jClick(languageLnkEle);
		jClick(currencyDrpdwnEle);
		switch (Currency) {
		case "USD":
			jClick(currencyUSDELe);
			break;
		case "MXN":
			jClick(currencyMXNDEle);
			break;
		case "CRC":
			jClick(currencyCRCEle);
			break;
		case "GTQ":
			jClick(currencyGTQEle);
			break;
		case "NIO":
			jClick(currencyNIOEle);
			break;
		}
	}
	
	public By selectDateMonthMob(String date) {
        String month = getElement(By.xpath("//*[@class='ui-datepicker-month']")).getText();
        String calYear = getElement(By.xpath("//*[@class='ui-datepicker-year']")).getText();
        String dateSelect = date.substring(0, 1).equals("0") ? date.substring(1, 2):date.substring(0, 2);
        String monthD = date.substring(3, date.length()-5).trim();
        String yearD = date.substring(date.length()-4).trim();
        while(!(monthD.equalsIgnoreCase(month)|| monthD.equalsIgnoreCase(monthSpanish(month))) && yearD.equals(calYear)) {
            getElement(datePickerEle).click();
            month = getElement(By.xpath("//*[@class='ui-datepicker-month']")).getText();
            calYear = getElement(By.xpath("//*[@class='ui-datepicker-year']")).getText();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int num = 0;
        try {
            Date monthNum = new SimpleDateFormat("MMMM").parse(month);
             num = Integer.valueOf(new SimpleDateFormat("MM").format(monthNum));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dateFormat = String.format("//a[text()='%s']/parent::td[@data-month='%s']/a", dateSelect, num - 1);
        return By.xpath(dateFormat);
    }

 private String monthSpanish(String month) {
        String spanishMonth = "";
        switch(month.toLowerCase()) {
            case "january":
                spanishMonth = "Enero";
                break;
            case "february":
                spanishMonth = "Febrero";
                break;
            case "march":
                spanishMonth = "Marzo";
                break;
            case "april":
                spanishMonth = "Abril";
                break;
            case "may":
                spanishMonth = "Mayo";
                break;
            case "june":
                spanishMonth = "Junio";
                break;
            case "july":
                spanishMonth = "Julio";
                break;
            case "august":
                spanishMonth = "Agosto";
                break;
            case "september":
                spanishMonth = "Septiembre";
                break;
            case "october":
                spanishMonth = "Octubre";
                break;
            case "november":
                spanishMonth = "Noviembre";
                break;
            case "december":
                spanishMonth = "Diciembre";
                break;                
        }
        return spanishMonth;
    }	
}
