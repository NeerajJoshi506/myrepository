package com.framework.inputs;

import com.framework.commonReusableClasses.SeleniumUtilities;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Data {
    @DataProvider(name = "readExcel")
    public static Object[][] createData() throws IOException {
        Object[][] arrayObject = getExcelData();
        return  (arrayObject);
    }

    public static String[][] getExcelData() throws IOException {
        String[][] arrayExcelData = null;
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\data.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        int totalRows = sheet.getLastRowNum();
        int totalCols = sheet.getRow(0).getLastCellNum();
        arrayExcelData = new String[totalRows][totalCols];
        for (int i = 0; i <= totalRows; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a = new String[rs.getLastCellNum()];
                for (int j = 0; j < totalCols; j++) {
                    Object pp = SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                    arrayExcelData[i - 1][j] = pp.toString();
                }
            } catch (NullPointerException ex) { }
        }
        return arrayExcelData;
    }

    @DataProvider(name = "readArrayListForWeb")
    public static Iterator<Object[]> createDataforWeb() throws IOException {
        //ArrayList<CsvData> csvData = new ArrayList<>();
        ArrayList<CsvData> data = new ArrayList<>();
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\dataWeb.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        int rowCount = sheet.getLastRowNum();
        for (int i = 1; i <= rowCount; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a = new String[row.getLastCellNum()];
                for (int j = 0; j < row.getLastCellNum(); j++) {
                    Object pp = SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                }
                data.add(new CsvData(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12]));
            } catch (NullPointerException ex) {
                System.out.println("Blank rows in excel - row number" + (i + 1));
            }
        }
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for (CsvData d : data) {
            dp.add(new Object[]{d});
        }
        return dp.iterator();
    }

    @DataProvider(name = "readArrayListForVClub")
    public static Iterator<Object[]> createDataforVClub() throws IOException {
        //ArrayList<CsvData> csvData = new ArrayList<>();
        ArrayList<CsvData> data = new ArrayList<>();
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\dataVClub.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        int rowCount = sheet.getLastRowNum();
        for (int i = 1; i <= rowCount; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a = new String[row.getLastCellNum()];
                for (int j = 0; j < row.getLastCellNum(); j++) {
                    Object pp = SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                }
                data.add(new CsvData(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18]));
            } catch (NullPointerException ex) {
                System.out.println("Blank rows in excel - row number" + (i + 1));
            }
        }
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for (CsvData d : data) {
            dp.add(new Object[]{d});
        }
        return dp.iterator();
    }

    @DataProvider(name = "readArrayListForAgency")
    public static Iterator<Object[]> createDataforAgency() throws IOException {
    	ArrayList<CsvData> data = new ArrayList<>();
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\dataAgency.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        int rowCount = sheet.getLastRowNum();
        for (int i = 1; i <= rowCount; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a = new String[row.getLastCellNum()];
                for (int j = 0; j < row.getLastCellNum(); j++) {
                    Object pp = SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                }
                //data.add(new CsvData(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13]));
                data.add(new CsvData(a[0], a[1], a[2],a[3],a[4],a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12],a[13]));
            } catch (NullPointerException ex) {
                System.out.println("Blank rows in excel - row number" + (i + 1));
            }
        }
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for (CsvData d : data) {
            dp.add(new Object[]{d});
        }
        return dp.iterator();
    }

}
