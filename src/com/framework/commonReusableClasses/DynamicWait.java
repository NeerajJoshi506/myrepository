package com.framework.commonReusableClasses;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.*;

public class DynamicWait extends BrowserSelection {
	
	 By loaderEle = By.xpath("//*[@id='system-loader']");	 
	 By loaderEleActive = By.xpath("//*[@id='system-loader']");
	 
	/**
	 * @param args
	 */
	 
	 int i = 0;
	 RemoteWebDriver driver;
	 WebDriverWait wait;
	 
	 public DynamicWait(RemoteWebDriver driver) {
         this.driver = driver;
     }
	 
	 public void checkPageLoader() throws InterruptedException {
		 for (int j = 0; j < 4; j++) {
			 int f = 0;
			 while(driver.findElement(loaderEle).getAttribute("style").equals("display: block;")) {
				 f++;
				 Thread.sleep(1000);
				 System.out.println("Loading loader found - " + f);
			 }
			Thread.sleep(1000);
		 }
	 }
	 
	 public void checkPageLoaderInvisiblity() throws InterruptedException {
		 driverWait(WaitConditions.INVISIBLE, loaderEle);
	 }
	 
	 public void checkPageLoaderInvisiblity(By by) throws InterruptedException {
		 driverWait(WaitConditions.INVISIBLE, by);
	 }
	 
	public  void webDriverWait(By by) {
		try {
			wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.elementToBeClickable(by));
		} catch (TimeoutException e) {
			System.out.println("There is some performance issue, page was not loaded at all.");
		}	
	}

	public  void waitFluent(By by) {
		Wait<WebDriver> waits = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		waits.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(by);
			}
		});
	}
	
	public enum WaitConditions {
        INVISIBLE,
        VISIBLE,
        PRESENCE,
        CLICKABLE
    }

    public  void driverWait(WaitConditions waitConditions, By loc) {
		wait = new WebDriverWait(driver, 180);
        switch (waitConditions) {
        case INVISIBLE:
            wait.until(ExpectedConditions.invisibilityOfElementLocated(loc));
            break;
        case VISIBLE:
            wait.until(ExpectedConditions.visibilityOfElementLocated(loc));
            break;
        case PRESENCE:
            wait.until(ExpectedConditions.presenceOfElementLocated(loc));
            break;
        case CLICKABLE:
            wait.until(ExpectedConditions.elementToBeClickable(loc));
            break;
        }
    }

	public  void driverWait(WaitConditions waitConditions, By loc, int timeout) {
		wait = new WebDriverWait(driver, timeout);
		switch (waitConditions) {
		case INVISIBLE:
			wait.until(ExpectedConditions.invisibilityOfElementLocated(loc));
			break;
		case VISIBLE:
			wait.until(ExpectedConditions.visibilityOfElementLocated(loc));
			break;
		case PRESENCE:
			wait.until(ExpectedConditions.presenceOfElementLocated(loc));
			break;
		case CLICKABLE:
			wait.until(ExpectedConditions.elementToBeClickable(loc));
			break;
		}
	}

	public  void waitForPageLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(pageLoadCondition);
	}
}
