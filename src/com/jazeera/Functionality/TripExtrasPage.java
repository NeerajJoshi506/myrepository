package com.jazeera.Functionality;

import com.framework.commonReusableClasses.ByXpath;
import com.framework.commonReusableClasses.DynamicWait;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TripExtrasPage extends ByXpath {
	
	public static String verifyHotel = "We are sorry";
	
	By tripExtraPageEle = By.xpath(".//*[@id='trip-extra-div']");
	By tripAssistanceEle = By.xpath("//*[@id='modal-protect-id']");
	By checkTripAssistanceEle = By.xpath("//*[@id='checkbox-extra-baggage checbox-baggage-amount']");
	By addTripAssistanceEle = By.xpath("//*[@id='modal-protecttrip']/div/div/div[2]/button");
	By activitiesEle = By.xpath("//*[@id='modal-actvities-selectEdit']");
	By selectActivitiesEle = By.xpath("//*[@id='activity-list']/div[1]/div[1]/div[2]/div[1]/div[3]/div/div[3]/button");
	By incRegChildEle = By.xpath("//*[@id='activity-list']/div[1]/div[2]/div[2]/div/div[1]/div[1]/div[2]/div/a[1]");
	By bookActivitiesToTripEle = By.xpath("//*[@id='activity-list']/div[1]/div[2]/div[3]/div/button");
	By submitBtnTripExtrasEle = By.xpath("//*[@id='submit_extras_button']");
	By verifyActivityEle = By.xpath("//*[@id='activity-div']/div[7]/h3");
	By activityCloseEle = By.xpath("//*[@id='modal-activities']/div/div/div[1]/button");
	By withoutAssitanceEle = By.xpath("//*[@*='cancel-link tripExtrasNoThanks']");	
	By priorityBoardingEle = By.xpath(".//*[@id='modal-priorityboarding-id']");
	By departureCheckedPriorityEle = By.xpath(".//tr[1]//*[contains(@id,'checkbox-priorityboarding')]");
	By returnCheckedPriorityEle = By.xpath(".//tr[2]//*[contains(@id,'checkbox-priorityboarding')]");
	By addPriorityModelEle = By.xpath("//button[contains(@class,'priorityboarding-button')]");
	By noThanksPriorityModelEle = By.xpath("//*[@id='modal-priorityboarding']//a[contains(@class,'cancel-link')]");
	By parkingEle = By.xpath("//*[@id='modal-parking-id']");
	By addParkingEle = By.xpath("//button[@class='general parking-button btn btn-base']");	
	By businessFastPassEle = By.xpath(".//*[@id='modal-businessFastPass-selectEdit']");
	By checkedBusiFastPassEle = By.xpath("//span[@class='checkbox-label amount']");
	By addBusiFastPassEle = By.xpath("//button[contains(@class,'businessFastPass-button')]");	
	By onTimePerforEle = By.xpath(".//*[@id='modal-ontime-id']");
	By checkedDeprtPerformEle = By.xpath(".//*[@id='checkbox-ontime-outbound checbox-amount-outbound']");
	By checkedReturnPerformEle = By.xpath(".//*[@id='checkbox-ontine-return checkbox-amount-return']");
	By addPerformEle = By.xpath(".//button[@class='btn-base ontime-button']");	
	By sportEquipEle = By.xpath(".//*[@id='modal-sportEquipment-selectEdit']");
	By listDeprtSportEle = By.xpath(".//*[@id='modal-sportEquipment']//button[@class='dropdown-toggle sports-button'][@tab-index='3']");
	By selectedSportsEquipmentEle = By.xpath(".//ul[@class='sortSelect dropdown-menu sportsDropDown']/li[2]/a[@tab-index='5']");
	By addsportsEquipmentEle = By.xpath(".//button[@class='btn-base sportEquipment-button']");
	By musicalEquipEle = By.xpath(".//button[@id='modal-musicalInstrument-selectEdit']");
	By musicalEquipIncrEle = By.xpath(".//*[@class='glyphicon glyphicon-menu-up from-ticket-count-up']");
	By addMusicalEquipEle = By.xpath(".//button[@class='btn-base addtotrip']");
	By shuttleEle = By.xpath(".//button[@class='modal-shuttle-id  btn-base']");
	By shuttleOriginEle = By.xpath(".//button[@id='from-button'][@class='dropdown-toggle']");
	By shuttleFromEle =  By.xpath(".//*[contains(@id,'modal-shuttle')]//ul[@class='from-select dropdown-menu']/li[2]/a");
	By shuttleIncrEle =  By.xpath(".//*[@id='modal-shuttle-CUN']//a[@tab-index='9']/span");
	By shuttleDestinationEle = By.xpath(".//button[@id='to-button']");
	By addShuttleEle = By.xpath(".//button[@class='btn-base addtotrip'][@tab-index='18']");
	By cielitoLimpioEle = By.xpath(".//button[@id='modal-cielito-id']");
	By cielitoLimpioRadioEle = By.xpath(".//span[@id='cielito-label-complete']");
	By addCielitoLimpioEle = By.xpath(".//button[@class='btn-base env-button']");
	By expressCheckInEle = By.xpath(".//button[@id='modal-worryFreeCheckIn-id']");
	By expressChkInJrnyChkbx = By.xpath(".//span[@id='checkbox-worryFreeCheckIn-outbound checbox-amount-outbound']");
	By expressChkInTnC = By.xpath(".//span[@id='checkbox-iAgree']");
	By addExpressChkIn = By.xpath(".//button[@class='general worryFreeCheckIn-button btn btn-base']");
	By closeExprsChkInTnC = By.xpath(".//button[@class='close worryFreeTerms']");
	By cBXElement = By.xpath(".//button[@id='modal-crossBoarderExpress-selectEdit']");
	By cbxCheckboxEle = By.xpath("//*[@id='modal-crossBoarderExpress']//span[@class='checkbox-label']");
	By addCBX =By.xpath(".//button[@class='crossBoarderExpress-button btn-base pull-right']");
	By volarisProtectionEle = By.xpath(".//button[@id='modal-insurance-selectEdit']");
	By volarisProtectionChkboxEle = By.xpath("//*[@id='modal-insurancePersonalItems']//span[@class='checkbox-label']");
	By addVolarisProtection = By.xpath(".//button[@class='btn-base ins-personal-button']");
	By closeVolarisProtection = By.xpath("//*[@id='modal-insurancePersonalItems']//button[@class='close']");
	By vipLounge = By.xpath(".//button[@id='modal-VIPPremiumLounge-selectEdit']");
	By vipLoungeCheckBoxEle = By.xpath("//*[@id='modal-VIPPremiumLounge']//span[@class='checkbox-label amount']");
	By addVipLounge = By.xpath(".//button[@class='general VIPPremiumLounge-button btn-base btn-violet']");
	By departureProtectionEle = By.xpath(".//button[@id='modal-noshowflight-id']");
	By departureProtectionCheckboxEle = By.xpath(".//span[@id='checkbox-noshowflight-outbound checbox-amount-outbound']");
	By addDepartureProtection = By.xpath(".//button[@class='btn-base noshowflight-button btn-link']");

	DynamicWait dynamicWait;

	public TripExtrasPage(RemoteWebDriver driver) {
		super(driver);
        dynamicWait = new DynamicWait(driver);
	}

	public void tripExtras(String ssr) throws InterruptedException {
		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, tripExtraPageEle);
		int withoutTripAssistance = 0;
		String tripExtras = ssr;
		String [] totalTripExtras = tripExtras.split(", ");
		List<String> assistanceList = Arrays.asList(totalTripExtras);
		Iterator<String> itr = assistanceList.iterator();
		while (itr.hasNext()) {
			switch (itr.next().toString().toUpperCase()) {
			case "":
				break;
			case "AINS":
				log1.info("Selecting TripAssistance");
				tripAssitance();
				withoutTripAssistance = 1;
				break;
			case "PRIO":
				log1.info("Priority Boarding");
				selectPriorityBoarding();
				break;			
			case "INST":
				log1.info("Musical Instrument");
				selectMusicalInstrument();
				break;
			case "SH":
				log1.info("Shuttles");
				selectShuttles();
				break;
			case "PRK":
				log1.info("Parking");
				selectParking();
				break;
			case "FAPA":
				log1.info("Business FastPass");
				selectBusinessFastPass();
				break;
			case "SPT":
				log1.info("Sports Equipment");
				selectSportsEquipment();
				break;
			case "OTPG":
				log1.info("On Time Performance");
				selectOnTimePerformance();
				break;
			case "CBOC":
				log1.info("Cielito Limpio");
				selectCielitoLimpio();
				break;
			case "WFCK":
				log1.info("Express check in");
				selectExpressCheckIn();
				break;
			case "CBX1":
				log1.info("Cross boarder express");
				selectCBX();
				break;			
			case "VP":
				log1.info("Volaris Protection");
				selectVolarisProtection();
				break;
			case "VIP":
				log1.info("VIP Lounge");
				selectVipLounge();
				break;
			case "NSBF":
				log1.info("Departure Protection");
				selectDepartureProtection();
				break;
			}
		}
		if (withoutTripAssistance == 0) {
            dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, submitBtnTripExtrasEle, 10);
			dynamicWait.checkPageLoaderInvisiblity();
			//Thread.sleep(2000);
			jClick(submitBtnTripExtrasEle);
			if(isObjectAvailable(withoutAssitanceEle))
				click(withoutAssitanceEle);
		} else
			jClick(submitBtnTripExtrasEle);
	}

	public void tripAssitance() throws InterruptedException {
		if(isObjectAvailable(tripAssistanceEle)) {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, tripAssistanceEle, 10);
			jClick(tripAssistanceEle);
			jClick(checkTripAssistanceEle);
			jClick(addTripAssistanceEle);
		} else
			System.out.println("Trip Assistance is not available");
	}
	
	public void selectPriorityBoarding() throws InterruptedException {
		if(isObjectAvailable(priorityBoardingEle, 2)) {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, priorityBoardingEle, 10);
			jClick(priorityBoardingEle);
			jClick(departureCheckedPriorityEle);
			jClick(addPriorityModelEle);
		} else
			System.out.println("Priority Boarding is not available");
	}
	
	public void selectParking() throws InterruptedException {
		if(isObjectAvailable(parkingEle, 2)) {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, parkingEle, 10);
			jClick(parkingEle);
			jClick(addParkingEle);
		} else
			System.out.println("Parking is not available");
	}
	
	public void selectBusinessFastPass() throws InterruptedException {
		if(isObjectAvailable(businessFastPassEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, businessFastPassEle, 10);
		jClick(businessFastPassEle);
		jClick(checkedBusiFastPassEle);
		jClick(addBusiFastPassEle);
		} else 
			System.out.println("Business Fast Pass is not available");
	}
	
	public void selectMusicalInstrument() throws InterruptedException {
		if(isObjectAvailable(musicalEquipEle, 2)) {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, musicalEquipEle, 10);
			jClick(musicalEquipEle);
			jClick(musicalEquipIncrEle);
			jClick(addMusicalEquipEle);
		}  else 
			System.out.println("Musical Equipments are not available");
	}
	
	public void selectShuttles() throws InterruptedException {
		if(isObjectAvailable(shuttleEle, 2)) {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, shuttleEle, 10);
			jClick(shuttleEle);
			jClick(shuttleOriginEle);
			click(shuttleFromEle);
			jClick(shuttleIncrEle);
			jClick(addShuttleEle);
		} else 
			System.out.println("Shuttles  are not available");		
	}
	
	public void selectSportsEquipment() throws InterruptedException {
		if(isObjectAvailable(sportEquipEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, sportEquipEle, 10);
		jClick(sportEquipEle);
		click(listDeprtSportEle);
		click(selectedSportsEquipmentEle);
		jClick(addsportsEquipmentEle);
		} else 
			System.out.println("Sports Equipment is not available");
	}
	
	public void selectOnTimePerformance() throws InterruptedException {
		if(isObjectAvailable(onTimePerforEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, onTimePerforEle, 10);
		jClick(onTimePerforEle);
		jClick(checkedDeprtPerformEle);
		if (isObjectAvailable(checkedReturnPerformEle, 2)){
			jClick(checkedReturnPerformEle);
		}
		jClick(addPerformEle);
		} else 
			System.out.println("On Time Performance is not available");
	}
	
	public void selectCielitoLimpio() throws InterruptedException {
		if(isObjectAvailable(cielitoLimpioEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, cielitoLimpioEle, 10);
		jClick(cielitoLimpioEle);
		jClick(cielitoLimpioRadioEle);		
		jClick(addCielitoLimpioEle);
		} else 
			System.out.println("Cielito Limpio is not available");
	}
	
	public void selectExpressCheckIn() throws InterruptedException {
		if(isObjectAvailable(expressCheckInEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, expressCheckInEle, 10);
		jClick(expressCheckInEle);
		jClick(expressChkInJrnyChkbx);		
		jClick(expressChkInTnC);
		jClick(addExpressChkIn);
		jClick(closeExprsChkInTnC);
		} else 
			System.out.println("Express Check In is not available");
	}
	
	public void selectCBX() throws InterruptedException {
		if(isObjectAvailable(cBXElement, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, cBXElement, 10);
		jClick(cBXElement);
		jClick(cbxCheckboxEle);		
		jClick(addCBX);		
		} else 
			System.out.println("CBX is not available");
	}
	
	public void selectVolarisProtection() throws InterruptedException {
		if(isObjectAvailable(volarisProtectionEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, volarisProtectionEle, 10);
		jClick(volarisProtectionEle);
		jClick(volarisProtectionChkboxEle);		
		jClick(addVolarisProtection);
		jClick(closeVolarisProtection);
		} else 
			System.out.println("Volaris Protection is not available");
	}
	
	public void selectVipLounge() throws InterruptedException {
		if(isObjectAvailable(vipLounge, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, vipLounge, 10);
		jClick(vipLounge);
		jClick(vipLoungeCheckBoxEle);		
		jClick(addVipLounge);		
		} else 
			System.out.println("Vip Lounge is not available");
	}
	
	public void selectDepartureProtection() throws InterruptedException {
		if(isObjectAvailable(departureProtectionEle, 2)) {
		dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, departureProtectionEle, 10);
		jClick(departureProtectionEle);
		jClick(departureProtectionCheckboxEle);		
		jClick(addDepartureProtection);		
		} else 
			System.out.println("Departure Protection is not available");
	}
}
