package com.framework.commonReusableClasses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class ByXpath extends BrowserSelection {
	WebElement element = null;
	Select select;
	public Actions act;
	public String xPath;
	protected Jexecutor jexecutor;
	JavascriptExecutor jse;

    public RemoteWebDriver driver;
    protected DynamicWait dynamicWait;

	public ByXpath(RemoteWebDriver driver) {
        this.driver = driver;	
        dynamicWait = new DynamicWait(driver);
    }

	public void moveToElement(WebElement element) {
		jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView()", element);
	}

	public void actionClick(By by) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, by);
			act = new Actions(driver);
			act.moveToElement(driver.findElement(by)).click().perform();
			log1.info("Clicked on: " + by);
		} catch (NoSuchElementException e) {
			log1.info(e.getMessage());
		}
	}

	public void sendEsc() {
		act = new Actions(driver);
		act.sendKeys(Keys.ESCAPE).build().perform();
	}

	public void sendTab() {
		act = new Actions(driver);
		act.sendKeys(Keys.TAB).build().perform();
	}
	public void sendSpace() {
		act = new Actions(driver);
		act.sendKeys(Keys.SPACE).build().perform();
	}

	public void sendArrowDown() {
		act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		System.out.println("Scroll Down");
	}

	public void downArrow(By by) {
		element = driver.findElement(by);
		element.sendKeys(Keys.ARROW_DOWN);
	}

	public void clickDownUntilVisible(By locator, By visibleELemntOnPage) throws InterruptedException {
		int count = 0;
		while (!isObjectAvailable(locator,1) && count <= 20) {
			downArrow(visibleELemntOnPage);
			Thread.sleep(5000);
			count++;
		}
	}

    public void jClick(By by) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, by, 60);
			element = driver.findElement(by);
			jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click();", element);			
			log1.info("Clicked on: " + by);
		} catch (NoSuchElementException e) {
			log1.info(e.getMessage());
		}
	}

	public void click(By by) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, by, 60);
			element = driver.findElement(by);
//			Jexecutor.highlightElement(element);
			element.click();
			log1.info("Clicked on: " + by);
		} catch (NoSuchElementException e) {
			log1.info(e.getMessage());
		}
	}

	public void sendKeys(By by, String data) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by, 5);
			element = driver.findElement(by);
			element.clear();
			element.sendKeys(data.trim());
			log1.info("Data send to control.");
		} catch (NoSuchElementException e) {
			log1.info(e.getMessage());
		}
	}

	public boolean isObjectAvailable(By by) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by);
			return  true;
		} catch (TimeoutException tm) {
			return false;
		}
	}

	public boolean isObjectDisplay(By by) {
		if(driver.findElement(by).isDisplayed()) {
			log1.info(by);
			return true;
		} else
			return false;			
	}

    public boolean isObjectAvailable(By by, int timeout) throws InterruptedException {
        try {
            dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by,timeout);
            log1.info(by);
            return  true;
        } catch (TimeoutException tm) {
            return false;
        }
    }

    public WebElement getElement(By locator) {
        try {
            int count = 0;
            while(driver.findElements(locator).size() == 0 && count < 30) {
                count++;
                Thread.sleep(1000);
                System.out.println("Waiting for " + locator + " for " + count + "000 ms");
            }
            return driver.findElement(locator);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
//            Assert.fail("Test Failed - Element not found - " + locator);
			System.out.println("Test Failed - Element not found - " + locator);
        } catch (NullPointerException ex) {
//            Assert.fail("Null Pointer exception - " + locator);
			System.out.println("Null Pointer exception - " + locator);
        } catch (StaleElementReferenceException ex) {
            System.out.printf("Element is not attached to the page document - " + ex.getStackTrace());
        } catch(Exception e) {
        	//System.out.println("Element " + locator + " was not clickable " + e.getStackTrace());
            System.out.println("Get Elment issue - " );
            e.printStackTrace();
        }
        return null;
    }

	public WebElement getElement(By locator, int waitTime) {
		try {
			int count = 0;
			while(driver.findElements(locator).size() == 0 && count < waitTime)  {
				count++;
				Thread.sleep(1000);
				System.out.println("Waiting for " + locator + " for " + count + "000 ms");
			}
			return driver.findElement(locator);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		} catch (NoSuchElementException ex) {
//            Assert.fail("Test Failed - Element not found - " + locator);
			System.out.println("Test Failed - Element not found - " + locator);
		} catch (NullPointerException ex) {
//            Assert.fail("Null Pointer exception - " + locator);
			System.out.println("Null Pointer exception - " + locator);
		} catch (StaleElementReferenceException ex) {
			System.out.printf("Element is not attached to the page document - " + ex.getStackTrace());
		} catch(Exception e) {
			//System.out.println("Element " + locator + " was not clickable " + e.getStackTrace());
			System.out.println("Get Elment issue - " );
			e.printStackTrace();
		}
		return null;
	}

   public int getLocatorSize(By locator) {
       try {
           int count = 0;
           while(driver.findElements(locator).size() == 0 && count < 30)  {
               count++;
               Thread.sleep(1000);
               System.out.println("Waiting for " + locator + " for " + count + "000 ms");
           }
           return driver.findElements(locator).size();
       } catch (InterruptedException ex) {
           ex.printStackTrace();
       } catch (NoSuchElementException ex) {
           Assert.fail("Test Failed - Element not found - " + locator);
       } catch (NullPointerException ex) {
           Assert.fail("Null Pointer exception - " + locator);
       } catch (StaleElementReferenceException ex) {
           System.out.printf("Element is not attached to the page document - " + ex.getStackTrace());
       } catch(Exception e) {
           //System.out.println("Element " + locator + " was not clickable " + e.getStackTrace());
           System.out.println("Get Elment issue - " );
           e.printStackTrace();
       }
       return 0;
   }

	public String getText(By by) throws InterruptedException {
		if (isObjectAvailable(by)) {
			String textvalue = null;
			element = driver.findElement(by);
			textvalue = element.getText();
			return textvalue;
		} else
			throw new NoSuchElementException("Element was not found on page.");
	}

	public void drpdwnByValue(By by, String visibleText) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by);
			element = driver.findElement(by);
//			Jexecutor.highlightElement(element);
			select = new Select(driver.findElement(by));
			select.selectByValue((visibleText.trim()));
			log1.info("Value Selected.");
		} catch (NoSuchElementException e) {
			log1.info(e.getMessage());
		}
	}

	public void drpdwnByText(By by, String visibleText) throws InterruptedException {
		try {
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by);
			element = driver.findElement(by);
//			Jexecutor.highlightElement(element);
			select = new Select(driver.findElement(by));
			select.selectByVisibleText((visibleText.trim()));
			log1.info("Value Selected.");
		} catch (NoSuchElementException e) {
			log1.info(e.getMessage());
		}
	}

	public String getAttributeValue(By by, String attributeName) throws InterruptedException {
		if (isObjectAvailable(by)) {
			String value = "";
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by);
			element = driver.findElement(by);
			value = element.getAttribute(attributeName.trim());
//			Thread.sleep(1500);
			return value;
		} else
			throw new NoSuchElementException("Element was not found on page.");
	}

    public void clearText(By by) throws InterruptedException {
    	if (isObjectAvailable(by)) {
			element = driver.findElement(by);
			element.clear();
		} else
			throw new NoSuchElementException("Element was not found on page.");
	}

	public By checkObject(String value) {
		String f = String.format(".//*[@id='unit1_9']/div/span[text()='%s']", value);
		return By.xpath(f);
	}

	public By selectDate(int monthIncrement, int dateIncrement) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE,  dateIncrement);
		int currentDate = calendar.get(Calendar.DATE);
		calendar.add(Calendar.MONTH, monthIncrement);
		int currentMonth = calendar.get(Calendar.MONTH);
		String dateFormat = String.format("//td[@data-month='%s']//a[text()='%s']", String.valueOf(currentMonth), String.valueOf(currentDate));
		return By.xpath(dateFormat);
	}

	public By selectDate1(String departureDate) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
		Calendar calendar = Calendar.getInstance();
		String currDate = simpleDateFormat.format(calendar.getTime());
		int dateIncrement = 0;
        try {
            dateIncrement = (int) (0 + (Math.abs(simpleDateFormat.parse(departureDate).getTime() - simpleDateFormat.parse(currDate).getTime()) / 86400000));
        } catch (ParseException ex) {
            ex.printStackTrace();
            dateIncrement =  0;
        }
		calendar.add(Calendar.DATE, dateIncrement );
		int currentDate = calendar.get(Calendar.DATE);
		calendar.add(Calendar.MONTH, 0);
		int currentMonth = calendar.get(Calendar.MONTH);
		String dateFormat = String.format("//td[@data-month='%s']//a[text()='%s']", String.valueOf(currentMonth), String.valueOf(currentDate));
		return By.xpath(dateFormat);
	}

    public void  switchToFrame(WebElement element) {
		driver.switchTo().frame(element);
	}

	public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

	public void goToUrl(String url) {
		driver.get(url);
	}

	public void windowHandle() throws InterruptedException {
		String parentWindow = driver.getWindowHandle();
		Set<String> set = driver.getWindowHandles();
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			if (!parentWindow.equals(childWindow)) {
				driver.close();
				driver.switchTo().window(childWindow);
				break;
			}
		}
	}
	
	public void windowHandle1() throws InterruptedException {
		String parentWindow = driver.getWindowHandle();
		Set<String> set = driver.getWindowHandles();
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			if (!parentWindow.equals(childWindow)) {				
				driver.switchTo().window(childWindow);
				driver.close();				
				driver.switchTo().window(parentWindow);
				break;
			}
		}
	}

	public void newWindowHandle() {
		String parentWindow = driver.getWindowHandle(); // get the current window handle
		for (String childWindow : driver.getWindowHandles()) {
			driver.switchTo().window(childWindow); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		driver.close(); // close newly opened window when done with it
		driver.switchTo().window(parentWindow);
	}
	
	public void elementList(By by) throws InterruptedException {
		/*if(isObjectAvailable(by))*/ {
			//dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, by);
	    	List<WebElement> list = driver.findElements(by);
	    	for(WebElement element: list) {
	    		Thread.sleep(1000);
	    		element.click();
	    		log1.info("clicked: " + by);
	    		break;
	    	}
		}
    }
	
	public void elementList1(By by) throws InterruptedException {
		if(isObjectAvailable(by)) {
	    	List<WebElement> list = driver.findElements(by);
	    	for(WebElement element: list) {
	    		jse.executeScript("arguments[0].click();", element);
	    		//element.click();
	    		log1.info("clicked: " + by);
	    		break;
	    	}
		}
    }
}
