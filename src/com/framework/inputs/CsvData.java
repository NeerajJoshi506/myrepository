package com.framework.inputs;

import com.framework.commonReusableClasses.SeleniumUtilities;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CsvData {
    BufferedReader bufferedReader;
    int col_num = 0;
    SimpleDateFormat dateFormat;

    private String testCaseID;
    private String execute;
    private String username;
    private String password;
    private String culture;
    private String currency;
    private String journeyType;
    private String origin;
    private String destination;
    private String departureDate;
    private String arrivalDate;
    private String adult;
    private String child;
    private String infant;
    private String car;
    private String hotel;
    private String ssr;
    private String flightType;
    private String paymentType;



    public String getTestCaseID() {
        return testCaseID;
    }
    
    public String getExecute() {
        return execute;
    }

    public String getUsename() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCulture() {
        return culture;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public String getJourneyType() {
        return journeyType;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public String getAdult() {
        return adult;
    }

    public String getChild() {
        return child;
    }

    public String getInfant() {
        return infant;
    }

    public String getPaymentType() {
        return paymentType;
    }
    
    public String getSsr() {
        return ssr;
    }
    
    public String getFlightType() {
        return flightType;
    }
    
    public String getCar() {
        return car;
    }
    
    public String getHotel() {
        return hotel;
    }

    public CsvData() {}
 
    /*----- For Web Anonymous User -----*/
    public CsvData(String testCaseID, String execute, String culture, String currency, String journeyType, String origin, String destination, String departureDate, String arrivalDate, String adult, String child, String infant,String paymentType) {
        this.testCaseID = testCaseID;
    	this.execute = execute;
        this.culture = culture;
        this.currency = currency;
        this.journeyType = journeyType;        
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.adult = adult;
        this.child = child;
        this.infant = infant;        
        this.paymentType = paymentType;
    }

    /*----- For Member User -----*/
    public CsvData(String testCaseID, String execute, String username, String password, String culture, String currency ,String journeyType, String flightType, String origin, String destination, String departureDate, String arrivalDate, String adult, String child, String infant, String ssr, String car, String hotel, String paymentType) {
    	this.testCaseID = testCaseID;
    	this.execute = execute;
        this.username = username;
        this.password = password;
        this.culture = culture;
        this.currency = currency;
        this.journeyType = journeyType;
        this.flightType = flightType; 
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.adult = adult;
        this.child = child;
        this.infant = infant;
        this.ssr = ssr;
        this.car = car;
        this.hotel = hotel;
        this.paymentType = paymentType;
    }
    /*----- For Agency -----*/
    public CsvData(String testCaseID, String execute,String username, String password, String currency, String journeyType, String origin, String destination, String departureDate, String arrivalDate, String adult, String child, String infant, String paymentType) {
    	this.testCaseID = testCaseID;
    	this.execute = execute;
    	this.currency = currency;
        this.username = username;
        this.password = password;               
        this.journeyType = journeyType;        
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.adult = adult;
        this.child = child;
        this.infant = infant;        
        this.paymentType = paymentType;
    }

    /*public ArrayList<CsvData> getCsvData() throws IOException {
        ArrayList<CsvData> collection = new ArrayList<>();
        String filePath = System.getProperty("user.dir") + "\\inputData\\data.csv";
        bufferedReader = new BufferedReader(new FileReader(filePath));
        String line = "";
        while((line = bufferedReader.readLine()) != null ) {
            String[] data = line.split(", ");
            journeyType = data[0];
            origin = data[1];
            destination = data[2];
            departureDate = data[3];
            arrivalDate = data[4];
            adult = data[5];
            child = data[6];
            infant = data[7];
            paymentType = data[8];
            collection.add(new CsvData(journeyType, origin, destination, departureDate, arrivalDate, adult, child, infant, paymentType));
        }
        return collection;
    }*/


    public ArrayList<CsvData> getCsvDataWihtoutTC() throws IOException {
        ArrayList<CsvData> collection = new ArrayList<>();
        String filePath = System.getProperty("user.dir") + "\\inputData\\data.csv";
        bufferedReader = new BufferedReader(new FileReader(filePath));
        String line = "";
        while((line = bufferedReader.readLine()) != null ) {
            String[] data = line.split(", ");
            journeyType = data[0];
            origin = data[1];
            destination = data[2];
            departureDate = data[3];
            arrivalDate = data[4];
            adult = data[5];
            child = data[6];
            infant = data[7];
            paymentType = data[8];
            //collection.add(new CsvData(journeyType, origin, destination, departureDate, arrivalDate, adult, child, infant, paymentType));
        }
        return collection;
    }

    public ArrayList<CsvData> excelReadWeb() throws IOException {
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\dataWeb.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        col_num = -1;
        int rowCount = sheet.getLastRowNum();
        ArrayList<CsvData> data = new ArrayList<>();
        for(int i=1; i <= rowCount; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a= new String[row.getLastCellNum()];
                for (int j = 0; j < row.getLastCellNum() ; j++) {
                    Object  pp =  SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                }
                data.add(new CsvData(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12]));
            }catch (NullPointerException ex) {
                System.out.println("Blank rows in excel - row number" + (i + 1));
            }
        }
        return data;
    }

    public ArrayList<CsvData> excelReadVClub() throws IOException {
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\dataVClub.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        col_num = -1;
        int rowCount = sheet.getLastRowNum();
        ArrayList<CsvData> data = new ArrayList<>();
        for(int i=1; i <= rowCount; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a= new String[row.getLastCellNum()];
                for (int j = 0; j < row.getLastCellNum() ; j++) {
                    Object  pp =  SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                }
                data.add(new CsvData(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14],a[15],a[16], a[17], a[18]));
            }catch (NullPointerException ex) {
                System.out.println("Blank rows in excel - row number" + (i+1));
            }
        }
        return data;
    }

    public ArrayList<CsvData> excelReadAgency() throws IOException {
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\inputData\\dataAgency.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        col_num = -1;
        int rowCount = sheet.getLastRowNum();
        ArrayList<CsvData> data = new ArrayList<>();
        for(int i=1; i <= rowCount; i++) {
            try {
                Row rs = sheet.getRow(i);
                String[] a= new String[row.getLastCellNum()];
                for (int j = 0; j < row.getLastCellNum() ; j++) {
                    Object  pp =  SeleniumUtilities.cellToType(rs.getCell(j));
                    a[j] = pp.toString();
                }
                data.add(new CsvData(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12]));
            }catch (NullPointerException ex) {
                System.out.println("Blank rows in excel - row number" + (i+1));
            }
        }
        return data;
    }

    public void writeData(String filename, String tcNumber, String tcDescription, String testStatus,  String PNR, String errorMessage) throws IOException,  InvalidFormatException {
        dateFormat = new SimpleDateFormat("DD MMM ");
        File fis = new File(System.getProperty("user.dir") + "\\inputData\\" + filename + ".xlsx");
        XSSFWorkbook workbook;
        XSSFSheet sheet;
        if(!fis.exists()) {
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet("Result");
        } else {
            FileInputStream finp = new FileInputStream(fis);
            workbook = new XSSFWorkbook(finp);
            sheet = workbook.getSheetAt(0);
        }
        int getLastRow = sheet.getLastRowNum();
        Row row = sheet.createRow(++getLastRow);
        int lastCell = row.getLastCellNum();
        Cell cell1 = row.createCell(lastCell + 1);
        Cell cell2 = row.createCell(lastCell + 2);
        Cell cell3 = row.createCell(lastCell + 3);
        Cell cell4 = row.createCell(lastCell + 4);
        Cell cell5 = row.createCell(lastCell + 5);
        cell1.setCellValue(tcNumber);
        cell2.setCellValue(tcDescription);
        cell3.setCellValue(testStatus);
        cell4.setCellValue(PNR);
        cell5.setCellValue(errorMessage);
        FileOutputStream outputStream = new FileOutputStream(fis);
        try {
            workbook.write(outputStream);
//            outputStream.flush();
            outputStream.close();
//            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
