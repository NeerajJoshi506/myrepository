package com.framework.inputs;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Results {


    private static String[] columns = {"Test Case ID", "Status", "PNR", "Error"};

    public String getTestCaseID() {
        return testCaseID;
    }

    public String getTcNumber() {
        return tcNumber;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public String getPNR() {
        return PNR;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private String testCaseID;
    private String tcNumber;
    private String testStatus;
    private String PNR;
    private String errorMessage;

    //private static String[] foldersName = {"Web","Vclub","Agency"};

    public Results(String testCaseID, String testStatus, String PNR, String errorMessage) {
        this.testCaseID = testCaseID;
        this.testStatus = testStatus;
        this.PNR = PNR;
        this.errorMessage = errorMessage;
    }

  /*  public static void writeResults(String filename, ArrayList<Results> results) throws IOException, InvalidFormatException {
        SimpleDateFormat smp = new SimpleDateFormat("DD_MMM_HH_mm");
        String fileExtension = System.getProperty("user.dir") + "\\TestResults\\" + filename + ".xlsx";
        File fis = new File(fileExtension);
        XSSFWorkbook workbook;
        XSSFSheet sheet;
        if(!fis.exists()) {
            fileExtension = System.getProperty("user.dir") + "\\inputData\\" + filename + smp.format(new Date()) + ".xlsx";
            fis = new File(fileExtension);
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet("Result");
        } else {
            FileInputStream finp = new FileInputStream(fis);
            workbook = new XSSFWorkbook(finp);
            sheet = workbook.getSheetAt(0);
        }
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
//        headerFont.setBoldweight((short) 1);
//        headerFont.setFontHeight((short) 16);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        // Create a Row
        Row headerRow = sheet.createRow(0);
        // Creating cells
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        int rowNum = 1;
        for(Results res:results) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(res.getTestCaseID());
            row.createCell(1).setCellValue(res.getTestStatus());
            row.createCell(2).setCellValue(res.getPNR());
            row.createCell(3).setCellValue(res.getErrorMessage());
        }
        // Resize all columns to fit the content size
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        FileOutputStream outputStream = new FileOutputStream(fis);
        try {
            workbook.write(outputStream);
//            outputStream.flush();
            outputStream.close();
//            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
   }*/

   public static String createExcelFile(String filename) throws IOException {
       //Create Test Results Folder if does not exist
        if(Files.exists(Paths.get( System.getProperty("user.dir") + "\\TestResults\\"))) {
            System.out.printf("");
        } else {
            File theFolder = new File("TestResults");
            try {
                theFolder.mkdir();
            } catch (SecurityException ex) {
                System.out.printf("");
            }
        }
       // Create Sub folder if does not exist
       if(!Files.exists(Paths.get(System.getProperty("user.dir")+ "\\TestResults\\" + filename))) {
           File file = new File(System.getProperty("user.dir")+ "\\TestResults\\" + filename);
           file.mkdir();
       }
       String fileExtension = System.getProperty("user.dir") + "\\TestResults\\" + filename + "\\" + filename +".xlsx";
       File fis = new File(fileExtension);
       SimpleDateFormat smp = new SimpleDateFormat("dd_MMM_yyyy_HH_mm");
       String date = smp.format(new Date());
       if(!fis.exists()) {
           fileExtension = System.getProperty("user.dir") + "\\TestResults\\" + filename + "\\" + filename + "_" + date + ".xlsx";
           fis = new File(fileExtension);
           XSSFWorkbook workbook = new XSSFWorkbook();
           XSSFSheet sheet = workbook.createSheet("Result");
           //Create heading for columns
           Font headerFont = workbook.createFont();
           headerFont.setColor(IndexedColors.RED.getIndex());
           // Create a CellStyle with the font
           CellStyle headerCellStyle = workbook.createCellStyle();
           headerCellStyle.setFont(headerFont);
           // Create a Row
           Row headerRow = sheet.createRow(0);
           for(int i = 0; i < columns.length; i++) {
               Cell cell = headerRow.createCell(i);
               cell.setCellValue(columns[i]);
               cell.setCellStyle(headerCellStyle);
           }
           FileOutputStream outputStream = new FileOutputStream(fis);
           workbook.write(outputStream);
           outputStream.close();
       }
       return filename + "_" + date;
   }

    public static void writeData(String className,String filename, String tcNumber, String testStatus, String PNR, String errorMessage) throws IOException, InvalidFormatException {
//        SimpleDateFormat smp = new SimpleDateFormat("dd_MMM_HH_mm");
        String fileExtension = System.getProperty("user.dir") + "\\TestResults\\" + className + "\\" + filename + ".xlsx";
//        String fileExtension = filename;
        File fis = new File(fileExtension);
        XSSFWorkbook workbook;
        XSSFSheet sheet;
        FileInputStream finp = new FileInputStream(fis);
        workbook = new XSSFWorkbook(finp);
        sheet = workbook.getSheetAt(0);
        int getLastRow = sheet.getLastRowNum();
        Row row = sheet.createRow(++getLastRow);
        int lastCell = row.getLastCellNum();
        Cell cell1 = row.createCell(lastCell + 1);
        Cell cell2 = row.createCell(lastCell + 2);
        Cell cell3 = row.createCell(lastCell + 3);
        Cell cell4 = row.createCell(lastCell + 4);
//        Cell cell5 = row.createCell(lastCell + 5);
        cell1.setCellValue(tcNumber);
        cell2.setCellValue(testStatus);
        cell3.setCellValue(PNR);
        cell4.setCellValue(errorMessage);
//        cell5.setCellValue(errorMessage);
        FileOutputStream outputStream = new FileOutputStream(fis);
        try {
            workbook.write(outputStream);
//            outputStream.flush();
            outputStream.close();
//            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
