package com.framework.commonReusableClasses;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.framework.inputs.CsvData;
import com.framework.inputs.Results;
import com.jazeera.Functionality.*;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.apache.log4j.Logger;
import org.testng.annotations.*;

import static com.framework.inputs.GlobalComponents.chromePath;
import static com.framework.inputs.GlobalComponents.geckoPath;
import static com.framework.inputs.GlobalComponents.iePath;
import static com.framework.inputs.GlobalComponents.safariPath;

public class BrowserSelection  {
	public String failure;
    public String pnr;
    public String status;
    public  String fileExcel;
    public ArrayList<Results> webStatus;

	public RemoteWebDriver driver;
	static WebElement element = null;
	public static Logger log1 = Logger.getLogger("rootLogger");

    protected CsvData flightData;
    protected HomePage homePage;
    protected FlightAvailabilityPage flightAvailabilityPage;
    protected PaxInfoPage paxInfoPage;
    protected SeatPage seatPage;
    protected TripExtrasPage tripExtraPage;
    protected CarAndHotelPage carAndHotelPage;
    protected LoginPage loginPage;
    protected PaymentPage paymentPage;
	protected DynamicWait dynamicWait;
    protected BrowserSelection browserSelection;
    ByXpath xpath;
    
    static DesiredCapabilities cap;;
    static ChromeOptions opt;

	@BeforeSuite(alwaysRun = true)
	public void setup() {
		browserSelection = new BrowserSelection();
		flightData = new CsvData();
	}

	@Parameters("browser")
	@BeforeMethod
    public void setEnv(String browser) throws IOException {
        startBrowser(browser);
        xpath = new ByXpath(driver);
        dynamicWait = new DynamicWait(driver);
        homePage = new HomePage(driver);
        flightAvailabilityPage = new FlightAvailabilityPage(driver);
        paxInfoPage = new PaxInfoPage(driver);
        seatPage = new SeatPage(driver);
        tripExtraPage = new TripExtrasPage(driver);
        carAndHotelPage = new CarAndHotelPage(driver);
        paymentPage = new PaymentPage(driver);
        loginPage = new LoginPage(driver);
    }

	
	@SuppressWarnings("deprecation")
	public void startBrowser(String browser) throws IOException {
    	//browser = "chrome";
		switch (browser.toLowerCase().trim()) {
		case "firefox":
			System.setProperty("webdriver.gecko.driver", geckoPath);
			driver = new FirefoxDriver();
			System.out.println("Running FireFox");
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "chrome":
			System.setProperty("webdriver.chrome.driver", chromePath);
			ChromeOptions opt = new ChromeOptions();
			opt.addArguments("disable-extensions");
			opt.addArguments("--start-maximized");
			opt.addArguments("disable-infobars");
			driver = new ChromeDriver(opt);
			System.out.println("Running Chrome");
			driver.manage().deleteAllCookies();
			break;
		case "ie" :
			System.setProperty("webdriver.ie.driver", iePath);
			cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			InternetExplorerDriverService.Builder ies = new InternetExplorerDriverService.Builder();
			InternetExplorerDriverService service = ies.build();
			driver = new InternetExplorerDriver(service, cap);
			System.out.println("Running IE");
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "safari" :
			System.setProperty("webdriver.safari.driver", safariPath);
			System.out.println("Running safari");
			driver = new SafariDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "android":
			System.out.println("Running android script");
			android();
			break;
		case "chromeEmulator":
			chromeEmulator();
			break;
		case "iPhone":
			iPhone();
			break;
		default:
			System.setProperty("webdriver.chrome.driver", chromePath);
			System.out.println("Running Chrome");
			opt = new ChromeOptions();
			opt.addArguments("disable-extensions");
			opt.addArguments("--start-maximized");
			opt.addArguments("disable-infobars");
			driver = new ChromeDriver(opt);
			driver.manage().deleteAllCookies();
			break;
		}
	}

	public void chromeEmulator() throws IOException {
    	System.setProperty("webdriver.chrome.driver", chromePath);
    	Map<String, String> mobileEmulation = new HashMap<String, String>();
    	mobileEmulation.put("deviceName", "Nexus 5");
    	opt = new ChromeOptions();
    	opt.addArguments("disable-infobars");
    	opt.addArguments("--start-maximized");
    	opt.setExperimentalOption("mobileEmulation", mobileEmulation);
    	driver = new ChromeDriver(opt);
    }

	public void android() throws IOException {
		cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);
		cap.setCapability("android-version", "1.4.0");
		cap.setCapability(CapabilityType.VERSION,"6.0.1");
		cap.setCapability("deviceName","ANDROID");
		cap.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT,true);
		cap.setCapability(CapabilityType.HAS_TOUCHSCREEN,true);
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);
		cap.setCapability("platformName","ANDROID");		
		driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	}

    public void iPhone() throws IOException {
        String nodeURL = "http://192.168.3.34:4723/wd/hub";
        cap = DesiredCapabilities.iphone();
        cap.setCapability("automationName" ,  "XCUITest");
        cap.setCapability("deviceName" ,  "iPhone");
        cap.setCapability("udid", "C8B37C1B-E7B6-4DE2-9D96-C503421D6B6C");
        cap.setCapability("platformName", "iOS");
        cap.setCapability("platformVersion", "11.2");
        cap.setCapability(CapabilityType.BROWSER_NAME, "safari");
        driver = new RemoteWebDriver(new URL(nodeURL), cap);
    }
    
    public void getURL() {
    	driver.get(ReadPropertyFile.read("applicationURL"));
    }    

    @AfterMethod
    public void quitDriver() throws IOException {
    	driver.quit();
    }
    
    public String getCurrentURL() {
    	String currentURL = driver.getCurrentUrl();
    	return currentURL;
    }
}
