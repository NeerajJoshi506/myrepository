package com.jazeera.Functionality;

import com.framework.commonReusableClasses.ByXpath;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeatPage extends ByXpath {
    By seatSelectionPageEle =By.xpath(".//*[@id='seatMapForm']");
    By nextSeatEle = By.xpath(".//*[@id='js-next-seatmap-button']");
    By continueSeatEle = By.xpath(".//a[@id='ControlGroupUnitMapView_UnitMapViewControl_LinkButtonAssignUnit']");
    By connctFlightSeatTxtEle = By.xpath(".//div[@class='details-section-container']/div[not(contains(@style, 'display: none'))]/div[@class='card-title']/div[not(contains(@style, 'display: none')) and (not(contains(@class, 'clearfix')))]");
    By gotSeatEle = By.xpath("//*[@*='seatmap-button btn-base' and @*='javascript:$.magnificPopup.close();']");
    By skipSeatPageEle = By.xpath("//*[@*='seatmap-button btn-responsive'][contains(@onclick,'TripExtras/Extras')]");
    By selectSeatEle = By.xpath("//*[@*='seatmap-button btn-responsive'][not(contains(@onclick,'/TripExtras/Extras'))]");
    
    String connctFlightSeatTxt = "WITH LAYOVER";

    public SeatPage(RemoteWebDriver driver) {
        super(driver);
    }

    public void seat() throws Exception {
//    	dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, seatSelectionPageEle);
    	dynamicWait.checkPageLoaderInvisiblity();
        log1.info("Selecting Seat.");
        //sendEsc();
        jClick(continueSeatEle);
        /*try {
            while(isObjectAvailable(nextSeatEle, 2)) {
                jClick(nextSeatEle);
                Thread.sleep(500);
            }
            jClick(continueSeatEle);
        } catch (NoSuchElementException ex) {
            throw new Exception("Seat page is not available.");
        }*/
    }
    
    public  void seatMob(String isSeat) throws Exception {
//    	dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, skipSeatPageEle);
    	dynamicWait.checkPageLoaderInvisiblity();
        switch(isSeat.toLowerCase()) {
        case "skip":
        	jClick(skipSeatPageEle);
//                log1.info("skipped seat page.");
            break;
        case "seat":
            log1.info("Selecting Seat.");
            jClick(selectSeatEle);
            while(isObjectAvailable(nextSeatEle, 2)) {
                jClick(nextSeatEle);
            }
            jClick(continueSeatEle);
            break;
        }
    }
}
