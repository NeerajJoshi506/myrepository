package com.framework.inputs;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GlobalComponents extends CsvData {
	public static boolean jsExecutor = false;
	//static Date dateobj = new Date();
    static SimpleDateFormat df = new SimpleDateFormat("dd_MMM_yyyy_HH_mm");
    static String date = df.format(new Date());
    public static SimpleDateFormat monthDay = new SimpleDateFormat("dd_MMM_yyyy");
	public static String commonDataRepository = System.getProperty("user.dir") + "\\inputData\\testData.properties";	
	public static String geckoPath = System.getProperty("user.dir") + "\\Drivers\\geckodriver.exe";	
	public static String chromePath = System.getProperty("user.dir") + "\\Drivers\\chromedriver.exe";	
	public static String iePath = System.getProperty("user.dir") + "\\Drivers\\IEDriverServer.exe";	
	public static String phantomJSPath = System.getProperty("user.dir") + "\\Drivers\\phantomjs.exe";
	public static String safariPath = System.getProperty("user.dir") + "\\Drivers\\SafariDriver.safariextz";
	public static String extentReportPath = System.getProperty("user.dir") + "\\ExtentReport\\";
	//public static String screenshotPath = System.getProperty("user.dir") + "\\ExtentReport\\Screenshot\\Screenshot_" + date + ".png";
	public static String screenshotPath = System.getProperty("user.dir") + "\\ExtentReport";
}
