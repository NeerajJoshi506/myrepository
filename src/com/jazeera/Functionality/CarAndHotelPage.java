package com.jazeera.Functionality;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import com.framework.commonReusableClasses.ByXpath;
import com.framework.commonReusableClasses.DynamicWait;

import org.openqa.selenium.remote.RemoteWebDriver;

public class CarAndHotelPage extends ByXpath {
	By carPageEle = By.xpath("//*[@id='modal-car']");
	By bookCarEle = By.xpath(".//*[@*='car-list']/div[1]//span[@id='firstPrice']/following-sibling::button");
	By carListEle = By.xpath(".//*[@*='car-list']//span[@id='firstPrice']/following-sibling::button"); 
	By bookHotelEle = By.xpath("//*[@id='hotel-list']/div[1]/div[1]/section[2]/input");
	By hotelListEle = By.xpath("//*[@id='hotel-list']//input[contains(@class,'js-hotel-selec')]");
	By addHotelListEle = By.xpath(".//div[@class='hotel-options']//input");
	By continueAddHotelEle = By.xpath(".//*[@id='hotel-modal-container']//input[@class='btn-base js-continue-modal']");
	By continueOnCarEle = By.xpath("//*[@id='submit_cars_button']");
	By hotelPageEle = By.xpath("//*[@id='hotels-div']");
	By hotelSubmitEle = By.xpath("//*[@id='submit_hotels_button']");
	By noHotelListEle = By.xpath(".//*[@id='hotel-list']/div[1]/h3");
	By invexCardNoThanksPopUpEle = By.xpath("//*[@id='js-bannerViewPopup']//a[@href='/Payment/New']");
	By hotelNoThanksEle = By.xpath(".//*[@id='hotel-onLoad-modal']//*[@value='No thanks' or @value='No me interesa']");
	By invexCardpopup = By.xpath("//*[@id='js-bannerViewPopup']");

	 DynamicWait dynamicWait;

	 public CarAndHotelPage(RemoteWebDriver driver) {
		 super(driver);
		 dynamicWait = new DynamicWait(driver);
	 }

	public void carAndHotel() throws InterruptedException, IOException {
		try {
            if (isObjectAvailable(carPageEle)) {
                log1.info("Selecting car.");
                dynamicWait.checkPageLoaderInvisiblity();
                jClick(continueOnCarEle);
                dynamicWait.checkPageLoaderInvisiblity();
            } if (isObjectAvailable(hotelPageEle, 10)) {
                log1.info("Selecting hotel.");
                jClick(hotelSubmitEle);
            } if (isObjectAvailable(invexCardNoThanksPopUpEle, 5))
            	jClick(invexCardNoThanksPopUpEle);
            else
            	System.out.println("");
        } catch (NoSuchElementException ex) {
            ex.printStackTrace();
        }
	}

	public void carAndHotelMob() throws InterruptedException, IOException {
		try {
            if (isObjectAvailable(carPageEle)) {
                log1.info("Selecting car.");
                jClick(continueOnCarEle);
            } if (isObjectAvailable(hotelPageEle, 10)) {
                log1.info("Selecting hotel.");
                //jClick(hotelSubmitEle);
                jClick(hotelNoThanksEle);
			} if (isObjectAvailable(invexCardNoThanksPopUpEle, 5))
				jClick(invexCardNoThanksPopUpEle);
			else
				System.out.println("");
        } catch (NoSuchElementException ex) {
            ex.printStackTrace();
        }
	}

	public void car(String bookCar) throws InterruptedException {
		dynamicWait.checkPageLoaderInvisiblity();
		if(isObjectAvailable(carPageEle)) {
			switch (bookCar.toLowerCase()) {
				case "no":
					break;
				case "yes":
					if(isObjectAvailable(bookCarEle, 10)) {
		                dynamicWait.checkPageLoaderInvisiblity();
		                elementList(carListEle);
					} else
						log1.info("Cars are not available.");
					break;
			}
			jClick(continueOnCarEle);
            dynamicWait.checkPageLoaderInvisiblity();
		}
	}

	public void hotel(String hotel) throws NoSuchElementException, InterruptedException {
		dynamicWait.checkPageLoaderInvisiblity();
		if(isObjectAvailable(hotelPageEle, 60)) {
			switch (hotel.toLowerCase()) {
			case "no":
				jClick(hotelSubmitEle);
				break;
			case "yes":
				if(isObjectAvailable(hotelListEle, 5)) {
					elementList(hotelListEle);
					elementList(addHotelListEle);
					dynamicWait.checkPageLoaderInvisiblity();
					click(continueAddHotelEle);
				} else {
					log1.info("Hotels are not available.");
					jClick(hotelSubmitEle);
				}
				break;
			}	
		} else
			System.out.println("Hotel page is not available for this root.");
		if (isObjectAvailable(invexCardNoThanksPopUpEle, 2))
			jClick(invexCardNoThanksPopUpEle);
		else
			System.out.println("");
	}
}
