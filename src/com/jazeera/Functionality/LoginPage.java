package com.jazeera.Functionality;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.framework.commonReusableClasses.ByXpath;
import com.framework.commonReusableClasses.DynamicWait;
import com.framework.commonReusableClasses.Jexecutor;
import com.framework.commonReusableClasses.ReadPropertyFile;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LoginPage extends ByXpath {
	WebElement element = null;
	By loginSignUpBtnEle = By.xpath("//*[@id='account']//div[@class='HomeAccount hidden-xs']/a[@data-mfp-src='#modcont-home-login']");
	By myAccountEle = By.xpath("//*[@id='account']/button");
	By signInMobileEle = By.xpath(".//*[@id='signin-div']/a[1]");
	By agentAcntLogoutEle = By.xpath("//a[@href='/Member/Logout']");
	By unameEle = By.xpath("//*[@id='vClubMember_username']");
	By passEle = By.xpath("//*[@id='vClubMember_password']");
	By agencyUserEle = By.xpath("//*[@id='ControlGroupLoginAgentView_AgentLoginView_TextBoxUserID']");
	By agencyPassEle= By.xpath("//*[@id='ControlGroupLoginAgentView_AgentLoginView_PasswordFieldPassword']");
	By agentBookingEle = By.xpath("//*[@id='NEWBOOKINGLINK']");
	By capchaEle = By.xpath("//*[@id='recaptcha-anchor']/div[@class='recaptcha-checkbox-checkmark']");
	By loginMemberSubmitEle = By.xpath("//*[@id='loginModalButton']");
	By loginAgencySubmitEle = By.xpath("//*[@id='ControlGroupLoginAgentView_AgentLoginView_LinkButtonLogIn']");
	By memberLogin = By.xpath("//*[@id='modcont-home-login']//input[@id='Radio_Signin_Volaris_Member_VClub']/following-sibling::span");
	By agencyLogin = By.xpath("//a[contains(text(),'Travel Agent Login')]");
	By iFrameMember = By.xpath("//div[@data-formid='vclubmemberform']//iframe[contains(@src, 'https://www.google.com/recaptcha/api2/')]");
	By iFrameAgency = By.xpath("//div[@data-formid='agencyMemberForm']//iframe[contains(@src, 'https://www.google.com/recaptcha/api2/')]");

	Jexecutor jexecutor;
	public LoginPage(RemoteWebDriver driver) {
		super(driver);
		jexecutor = new Jexecutor(driver);
	}	
	
	public void loginCaptchaClick(String memberType, By xpath) throws InterruptedException {
		switch(memberType) {
		case "member":
			element = getElement(iFrameMember);
			break;
		case "agency":
			element = getElement(iFrameAgency);
			break;
		}
		switchToFrame(element);
		click(xpath);
		switchToDefaultContent();
	}

	public void login(String memberType) throws InterruptedException {		
		jClick(loginSignUpBtnEle);
		log1.info("Clicking for Login the User/Member.");
		if (memberType.equalsIgnoreCase("member")) {
			sendKeys(unameEle, ReadPropertyFile.read("memberUser"));
			sendKeys(passEle, ReadPropertyFile.read("memberPass"));
			loginCaptchaClick(memberType, capchaEle);			
			actionClick(loginMemberSubmitEle);
			jexecutor.scrollUp();
		} if (memberType.equalsIgnoreCase("agency")) {
			jClick(agencyLogin);
			sendKeys(agencyUserEle, ReadPropertyFile.read("agencyUser"));
			sendKeys(agencyPassEle, ReadPropertyFile.read("agencyPass"));
			//loginCaptchaClick(memberType, capchaEle);		
			actionClick(loginAgencySubmitEle);
			//jexecutor.scrollUp();
		}
	}

	public void loginAccount(String memberType, String uid, String pwd) throws InterruptedException {
		//jClick(loginSignUpBtnEle);
		log1.info("Clicking for Login the User/Member.");
		if (memberType.equalsIgnoreCase("member")) {		
			sendKeys(unameEle, uid);
			sendKeys(passEle, pwd);
			loginCaptchaClick(memberType, capchaEle);
			jexecutor.scrollDown();
			click(loginMemberSubmitEle);
		}
		if (memberType.equalsIgnoreCase("agency")) {
			jClick(agencyLogin);
			sendKeys(agencyUserEle, uid);
			sendKeys(agencyPassEle, pwd);
			//loginCaptchaClick(memberType, capchaEle);		
			actionClick(loginAgencySubmitEle);
			dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE,agentBookingEle);
			click(agentBookingEle);
			//jexecutor.scrollUp();
		}
		//dynamicWait.checkPageLoaderInvisiblity();
	}

	public void loginAccountMob(String memberType, String uid, String pwd) throws InterruptedException {
		jClick(myAccountEle);
		log1.info("Clicking for Login the User/Member.");
		jClick(signInMobileEle);
		if (memberType.equalsIgnoreCase("member")) {		
			sendKeys(unameEle, uid);
			sendKeys(passEle, pwd);
			loginCaptchaClick(memberType, capchaEle);
			actionClick(loginMemberSubmitEle);
			jexecutor.scrollUp();
		}
		if (memberType.equalsIgnoreCase("agency")) {
			jClick(agencyLogin);
			sendKeys(agencyUserEle, uid);
			sendKeys(agencyPassEle, pwd);
			loginCaptchaClick(memberType, capchaEle);
			actionClick(loginAgencySubmitEle);
			jexecutor.scrollUp();
		}
		dynamicWait.checkPageLoaderInvisiblity();
	}

	public void acntLogout() throws InterruptedException {
		jClick(myAccountEle);
		jClick(agentAcntLogoutEle);
	}
}
