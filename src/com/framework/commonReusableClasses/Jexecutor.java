package com.framework.commonReusableClasses;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.ExecuteMethod;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteTouchScreen;
import org.openqa.selenium.remote.RemoteWebDriver;
import static com.framework.inputs.GlobalComponents.jsExecutor;

public class Jexecutor extends BrowserSelection {
    private RemoteWebDriver driver;
    private JavascriptExecutor js;
    
    public Jexecutor(RemoteWebDriver driver) {
    	this.driver = driver;
    }

	public  void highlightElement(WebElement element) throws InterruptedException {
		if (jsExecutor) {
			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
					"color: brown; border: 4px solid black;");
			Thread.sleep(100);
		}
	}

	public  void hidePopup(WebElement element)  {
		((JavascriptExecutor) driver).executeScript("arguments[0].style.visibility='hidden';", element);
	}

	public  void setAttributeValue(By by, String attributeName, String attributeValue) throws NoSuchElementException, InterruptedException {

		ByXpath xpath = new ByXpath(driver);
		if (xpath.isObjectAvailable(by)) {
			js = (JavascriptExecutor) driver;
			element = driver.findElement(by);
			js.executeScript("arguments[0].setAttribute('" + attributeName + "', '" + attributeValue + "')", element);
			//log1.info("Attribut value set.");
		} else
			throw new NoSuchElementException("Element was not found on page.");
	}

	public  void scrollDown() {
		js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,250)", "");
	}

	public  void scrollUp() throws InterruptedException {
		js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-500)", "");
		Thread.sleep(2000);
	}

	public  void scrollToEnd() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void mobileScrollDown() {
		ExecuteMethod method = new RemoteExecuteMethod(driver);
		RemoteTouchScreen screen = new RemoteTouchScreen(method);
		System.out.println("Scroll down");
		screen.down(50, 100);
	}
}
