package com.jazeera.testscripts;

import com.aventstack.extentreports.Status;
import com.framework.commonReusableClasses.ExtentReporting;
import com.framework.inputs.CsvData;
import com.framework.inputs.Data;
import com.framework.inputs.Results;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.*;
import static com.framework.inputs.GlobalComponents.extentReportPath;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

public class WebAndroid extends ExtentReporting {
	
	String className = this.getClass().getSimpleName();

    @BeforeClass
    public void setupFile() throws InvalidFormatException, IOException {
        fileExcel = Results.createExcelFile("webAndroid");
        webStatus = new ArrayList<>();
        extentReportName(extentReportPath +"\\" + className + "\\" + "ExtentReport_" + fileExcel + ".html");
    }

    @DataProvider
    @Test(dataProvider = "readArrayListForWeb", dataProviderClass = Data.class)
    public void BookingFlowAndroid(CsvData tcData) throws Exception {
    	failure="";
    	pnr="";
    	status="";
        try {
        	if(tcData.getExecute().equalsIgnoreCase("Yes")) {
        		System.out.println("Executing the " + className + " test case- " + tcData.getTestCaseID());
        	    getURL();
        	} else {
        		status = "SKIPPED";
        		throw new SkipException("");
        	}
        	test = extent.createTest("TestCase: " + tcData.getTestCaseID() + " paymentType: " + tcData.getPaymentType());
        	homePage.selectCultureAndCurrency(tcData.getCulture(), tcData.getCurrency());
            homePage.searchFlightsMob(tcData.getOrigin(), tcData.getDestination(), tcData.getJourneyType(), tcData.getDepartureDate(), tcData.getArrivalDate(), tcData.getAdult(), tcData.getChild(), tcData.getInfant());
            flightAvailabilityPage.selectFlightsMob(tcData.getJourneyType());
            //paxInfoPage.fillPaxinfoAll();
            seatPage.seatMob("skip");
            tripExtraPage.tripExtras("");
            carAndHotelPage.carAndHotelMob();
            paymentPage.paymentType(tcData.getPaymentType());
            pnr = paymentPage.confirmationBooking();
            test.log(Status.PASS, "PNR:- " + pnr);
            System.out.println(pnr);
            Assert.assertTrue(true);
        } catch (SkipException sk) {
            status = "SKIPPED";
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            failure = sw.toString();
            System.out.println(sw);
            status="FAIL";
            failure = getCurrentURL();
            Assert.fail(failure);
        } finally {
        	if (failure.isEmpty() && status.equalsIgnoreCase("SKIPPED")) {
                Results.writeData(className, fileExcel, "TC - " + tcData.getTestCaseID(), status, pnr, "");
        	} else {
        		if(failure.isEmpty()) {
        			Results.writeData(className, fileExcel, "TC - " + tcData.getTestCaseID(), "Pass", pnr, "");
                    test.pass("Test case passed.");
        		} else {
        			Results.writeData(className, fileExcel, "TC - " + tcData.getTestCaseID(), status, pnr, failure);
        			test.fail("Test case failed on following URL: " + failure);
        			takeScreenshot(className, fileExcel, tcData.getTestCaseID());
        		}
        	}
        }
    }

	@AfterClass
	public void saveExtent() {
		extent.flush();
	}    
}

