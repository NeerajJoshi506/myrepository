package com.jazeera.Functionality;

import com.framework.commonReusableClasses.*;
import com.framework.inputs.CsvData;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class PaxInfoPage extends ByXpath {
	WebElement ele = null;
	String startXpath = ".//*[@id='volarisPassengers_";
	String endFNameEle = "__Name_First']";
	String endLNameEle = "__Name_Last']";
	String endDayEle = "__day']";
	String endMonthEle = "__month']";
	String endYearEle = "__year']";
	By paxDetails = By.xpath(".//div[@id='passengerMainBody']");
	By paxTitleEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListTitle_0']");
	By firstNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxFirstName_0']");
    By lastNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxLastName_0']");
    By genderEle = By.xpath("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListGender_0']/option[2]");
    By childTitleEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListTitle_1']");
    By childFirstNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxLastName_1']");
    By travellingWithEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListAssign_0_0']");
    By infantSectionEle =By.xpath("//*[@id='passengerInputContainer0'][@class='section nk..9']");
    By infantLastNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxLastName_0_0']");
    By infantGenderEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListGender_0_0']");
    By infantDobDayEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListBirthDateDay_0_0']");
    By infantDobMonthEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListBirthDateMonth_0_0']");
    By infantDobYearEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListBirthDateYear_0_0']");
    By agencyInformationTxtEle = By.xpath("//*[@id='selectMainBody']/div[2]/div/div[1]/h2");
    By guestPhoneCodeEle1 = By.xpath("//*[@id='CONTROLGROUPCONTACT_ContactInputView_DROPDOWNLISTHOMEPHONECOUNTRYCODE']");
    By guestPhoneOriginEle = By.xpath("//*[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxHomePhone']");
    By guestPhoneCodeEle2 = By.xpath("//*[@id='CONTROLGROUPCONTACT_ContactInputView_DROPDOWNLISTMOBILECOUNTRYCODE']");
    By guestPhoneCodeDestEle = By.xpath("//*[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxOtherPhone']");
    By contactInfoTitleEle = By.xpath("//*[@id='CONTROLGROUPCONTACT_ContactInputView_DropDownListTitle']/option[3]");
    By contactFirstNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxFirstName']");
    By contactLastNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxLastName']");
    By cityNameEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxCity']");
    By countryNameEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_ContactInputView_DropDownListCountry']");
    By countryCodeEle = By.xpath(".//select[@id='CONTROLGROUPCONTACT_ContactInputView_DROPDOWNLISTMOBILECOUNTRYCODE']");
    By mobNoEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxOtherPhone']");
    By eMailEle = By.xpath(".//input[@id='CONTROLGROUPCONTACT_ContactInputView_TextBoxEmailAddress']");
    By continueEle = By.xpath(".//a[@id='CONTROLGROUPCONTACT_ButtonSubmit'][@class='button']");
    

    DynamicWait dynamicWait;

    public PaxInfoPage(RemoteWebDriver driver) {
        super(driver);
        dynamicWait = new DynamicWait(driver);
    }
/*
	public void fillPaxinfoAll() throws InterruptedException {
		dynamicWait.checkPageLoaderInvisiblity();
		log1.info("Filling pax info.");
//		dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, paxPageEle);
        if (isObjectAvailable(paxPageEle)) {
            int paxCount = getLocatorSize(passangerCountEle);
            int paxDiv = 0;
            int infantCount = 0;
            if (paxCount >= 1) {
                for (int initialValue = 0; initialValue < paxCount; initialValue++) {
                	paxDiv = initialValue + 1;
                    ele = getElement(By.xpath(".//*[@id='passenger-forms']/div[1]/div[" + paxDiv + "]"));
                    String paxType = getElement(By.xpath("//*[@id='passenger-forms']/div/div[@class='panel panel-default'][" + paxDiv + "]//span[contains(@class, 'passenger-type')]")).getText();
                    String isAdultExpanded =getAttributeValue(By.xpath("//*[contains(@class, 'panelLink')  and @aria-controls='collapse" + paxDiv + "']"), "aria-expanded");
                    if(isAdultExpanded.equals("false")) {
                        click(By.xpath("//*[@id='passenger-forms']/div/div[@class='panel panel-default'][" + paxDiv +"]//*[@class='panel-title']/a"));
                    }
                    String type = paxType.substring(0, 6).trim();
                    String day = "";
                    String month = "";
                    String year = "";
                    if(type.equalsIgnoreCase("Infant") || type.equalsIgnoreCase("Infante")) {
                        sendKeys(By.xpath(startXpath + "Infants_" + infantCount + endFNameEle), 
                                RandomData.getSaltString());
                        sendKeys(By.xpath(startXpath + "Infants_" + infantCount + endLNameEle), 
                                "TEST");
                        day = ReadPropertyFile.read("infantDay");
                        month = ReadPropertyFile.read("infantMonth");
                        year = ReadPropertyFile.read("infantYear");

                        drpdwnByText(By.xpath(startXpath + "Infants_" + infantCount + endDayEle), 
                                ReadPropertyFile.read("infantDay"));
                        drpdwnByText(By.xpath(startXpath + "Infants_" + infantCount + endMonthEle), 
                                ReadPropertyFile.read("infantMonth"));
                        drpdwnByText(By.xpath(startXpath + "Infants_" + infantCount + endYearEle), 
                                ReadPropertyFile.read("infantYear"));
                        infantCount++;
                    }else {
                        if(type.equalsIgnoreCase("Adult") || type.equalsIgnoreCase("Adulto")) {
                            day = ReadPropertyFile.read("day");
                            month = ReadPropertyFile.read("month");
                            year = ReadPropertyFile.read("year");
                        } else {
                            day = ReadPropertyFile.read("minorDay");
                            month = ReadPropertyFile.read("minorMonth");
                            year = ReadPropertyFile.read("minorYear");
                        }
                        sendKeys(By.xpath(startXpath + initialValue + endFNameEle), RandomData.getSaltString());
                        sendKeys(By.xpath(startXpath + initialValue + endLNameEle), "TEST" );
                        drpdwnByText(By.xpath(startXpath + initialValue + endDayEle), day);
                        drpdwnByText(By.xpath(startXpath + initialValue + endMonthEle), month);
                        drpdwnByText(By.xpath(startXpath + initialValue + endYearEle), year);
                    }
                }
            }
//****************Filling primary contact information*****************
           fillPrimaryContactInfo();
        } else
            throw new RuntimeException("Pax page is not available!");
    }


    public void fillPaxinfoAllForVclub() throws InterruptedException {
    	dynamicWait.checkPageLoaderInvisiblity();
    	log1.info("Filling pax info.");
//        dynamicWait.driverWait(DynamicWait.WaitConditions.VISIBLE, paxPageEle);
        if (isObjectAvailable(paxPageEle)) {
            int paxCount = getLocatorSize(passangerCountEle);
            int paxDiv = 1;
            int infantCount = 0;
            if (paxCount > 1) {
                for (int initialValue = 1; initialValue < paxCount; initialValue++) {
                	paxDiv = initialValue + 1;
                    ele = getElement(By.xpath(".//*[@id='passenger-forms']/div[1]/div[" + paxDiv + "]"));
                    String paxType = getElement(By.xpath("//*[@id='passenger-forms']/div/div[@class='panel panel-default'][" + paxDiv + "]//span[contains(@class, 'passenger-type')]")).getText();
                    String isAdultExpanded =getAttributeValue(By.xpath("//*[contains(@class, 'panelLink')  and @aria-controls='collapse" + paxDiv + "']"), "aria-expanded");
                    if(isAdultExpanded.equals("false")) {
                        click(By.xpath("//*[@id='passenger-forms']/div/div[@class='panel panel-default'][" + paxDiv +"]//*[@class='panel-title']/a"));
                    }
                    String type = paxType.substring(0, 6).trim();
                    String day = "";
                    String month = "";
                    String year = "";
                    if(type.equalsIgnoreCase("Infant") || type.equalsIgnoreCase("Infante")) {
                        sendKeys(By.xpath(startXpath + "Infants_" + infantCount + endFNameEle), 
                                RandomData.getSaltString());
                        sendKeys(By.xpath(startXpath + "Infants_" + infantCount + endLNameEle), 
                                "TEST");
                        day = ReadPropertyFile.read("infantDay");
                        month = ReadPropertyFile.read("infantMonth");
                        year = ReadPropertyFile.read("infantYear");
                        drpdwnByText(By.xpath(startXpath + "Infants_" + infantCount + endDayEle), 
                                ReadPropertyFile.read("infantDay"));
                        drpdwnByText(By.xpath(startXpath + "Infants_" + infantCount + endMonthEle), 
                                ReadPropertyFile.read("infantMonth"));
                        drpdwnByText(By.xpath(startXpath + "Infants_" + infantCount + endYearEle), 
                                ReadPropertyFile.read("infantYear"));
                        infantCount++;
                    } else {
                        if(type.equalsIgnoreCase("Adult") || type.equalsIgnoreCase("Adulto")) {
                            day = ReadPropertyFile.read("day");
                            month = ReadPropertyFile.read("month");
                            year = ReadPropertyFile.read("year");
                        } else {
                            day = ReadPropertyFile.read("minorDay");
                            month = ReadPropertyFile.read("minorMonth");
                            year = ReadPropertyFile.read("minorYear");
                        }
                        sendKeys(By.xpath(startXpath + initialValue + endFNameEle), RandomData.getSaltString());
                        sendKeys(By.xpath(startXpath + initialValue + endLNameEle), "TEST" );
                        drpdwnByText(By.xpath(startXpath + initialValue + endDayEle), day);
                        drpdwnByText(By.xpath(startXpath + initialValue + endMonthEle), month);
                        drpdwnByText(By.xpath(startXpath + initialValue + endYearEle), year);
                    }
                }
            }// ****************Filling primary contact information*****************
            fillPrimaryContactInfo();
        } else
            throw new RuntimeException("Pax page is not available!");
    }
 */ 
    public void fillPaxdetails(String adult,String child, String infant) throws InterruptedException {
    	if(isObjectAvailable(paxDetails,1)) {    		
    		int totalAdult = Integer.parseInt(adult);
    		int totalChild = Integer.parseInt(child);
    		int totalInfant = Integer.parseInt(infant);
    		int totalPax =  totalAdult + totalChild ;
    		for(int initial = 0; initial < totalPax; initial++){
    			String paxNo = new Integer(initial).toString();
    			String titleXpath = String.format(".//select[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListTitle_%s']/option[2]",paxNo);
    			String firstName = String.format(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxFirstName_%s']",paxNo );
    			String lastName = String.format(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxLastName_%s']",paxNo );
    			String gender = String.format("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListGender_%s']/option[text()='Male']",paxNo);
    			String genderLabelEle = String.format("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListGender_%sLabel']",paxNo);
    			By genderEle= By.xpath(genderLabelEle);
    			
    			click(By.xpath(titleXpath));    			
        		sendKeys(By.xpath(firstName), RandomData.getSaltString());
        		sendKeys(By.xpath(lastName), ReadPropertyFile.read("last"));
        		//click(By.xpath(gender));
        		if(isObjectAvailable(genderEle,1)){
        		click(By.xpath(gender));
        		} else{ System.out.println("Element not available!");}
    		}
    		if(totalInfant>0){
    			if(isObjectAvailable(infantSectionEle)){
    				for(int initial = 0; initial < totalInfant; initial++){
    	    			String paxNo = new Integer(initial).toString();
    	    			String firstName = String.format(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxFirstName_%s_%s']",paxNo,paxNo);
    	    			String lastName = String.format(".//input[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_TextBoxLastName_%s_%s']",paxNo,paxNo );
    	    			String gender = String.format("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListGender_%s_%s']/option[text()='Male']",paxNo,paxNo );
    	    			String day = String.format("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListBirthDateDay_%s_%s']/option[3]",paxNo,paxNo);
    	    			String month = String.format("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListBirthDateMonth_%s_%s']/option[6]",paxNo,paxNo);
    	    			String year = String.format("//*[@id='CONTROLGROUPCONTACT_CONTROLGROUPPASSENGER_PassengerInputViewContactView_DropDownListBirthDateYear_%s_%s']/option[3]",paxNo,paxNo);
    	    			   	    			
    	        		sendKeys(By.xpath(firstName), RandomData.getSaltString());
    	        		sendKeys(By.xpath(lastName), ReadPropertyFile.read("last"));
    	        		click(By.xpath(gender));
    	        		click(By.xpath(day));
    	        		click(By.xpath(month));
    	        		click(By.xpath(year));
    				}
    			}
    		}
    		Thread.sleep(2000);
    		if(isObjectAvailable(agencyInformationTxtEle,3)) {
    			//click(guestPhoneCodeEle1);
    			drpdwnByText(guestPhoneCodeEle1, ReadPropertyFile.read("guestOriginCode"));
    			sendKeys(guestPhoneOriginEle, ReadPropertyFile.read("guestOriginPhone"));
    			click(guestPhoneCodeEle1);
    			drpdwnByText(guestPhoneCodeEle2, ReadPropertyFile.read("guestDestinationCode"));
    			sendKeys(guestPhoneCodeDestEle, ReadPropertyFile.read("guestDestinationPhone"));
    		}
    		else fillPrimaryContactInfo();				    		
    		click(continueEle);    		
    	}
    }
    
    private void fillPrimaryContactInfo() throws InterruptedException {
        log1.info("Filling primary contact information.");        
        jClick(paxTitleEle);
		drpdwnByText(paxTitleEle, ReadPropertyFile.read("title"));        
        click(countryNameEle);
        drpdwnByText(countryNameEle, ReadPropertyFile.read("countryName"));
        sendKeys(mobNoEle, ReadPropertyFile.read("phoneNumber"));
        sendKeys(eMailEle, ReadPropertyFile.read("eMail"));        
    }
}
