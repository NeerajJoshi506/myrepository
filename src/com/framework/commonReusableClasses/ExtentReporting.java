package com.framework.commonReusableClasses;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;
import static com.framework.inputs.GlobalComponents.screenshotPath;

public class ExtentReporting extends BrowserSelection {

    public  ExtentHtmlReporter htmlReporter;
	public  ExtentReports extent;
	public  ExtentTest test;
	
	public void extentReportName(String fileName) {
		extent = new ExtentReports();
		htmlReporter = new ExtentHtmlReporter(fileName);
        extent.attachReporter(htmlReporter);
	}

    public void takeScreenshot(String className, String fileName, String testcaseID) throws IOException {
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(screenshotPath + "\\" + className + "\\" + fileName + "_TC_" + testcaseID + ".png"));
        test.fail("Testcase failed.").addScreenCaptureFromPath(screenshotPath + "\\" + className + "\\" + fileName + "_TC_" + testcaseID + ".png");
    }
}
