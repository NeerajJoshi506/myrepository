package com.framework.inputs;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SetTestName {

    @Retention(RetentionPolicy.RUNTIME)
    public @interface SetTestDesc {
        int idx() default 0;
    }
}
