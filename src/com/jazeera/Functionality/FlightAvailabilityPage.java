package com.jazeera.Functionality;

import com.framework.commonReusableClasses.ByXpath;
import com.framework.commonReusableClasses.DynamicWait;
import com.framework.commonReusableClasses.Jexecutor;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

public class FlightAvailabilityPage extends ByXpath {

    By flightPageEle = By.xpath(".//*[@class='tabs search_result_holder']");
    By departFlightTabEle = By.xpath(".//*[@id='regular-fare-list-0']//a[@href='#trip_0_passFare_0']");
    By returnFlightTabEle = By.xpath(".//*[@id='regular-fare-list-1']//a[@href='#trip_1_passFare_1']");
    By departingFlightEle = By.xpath(".//*[@id='0']//div[@class='tab-content'][not(contains(@style,'display: none;'))]//tr[2]/td[contains(@class,'fare_header_basic')]//input[1]");
    By returningFlightEle = By.xpath(".//*[@id='1']//div[@class='tab-content'][not(contains(@style,'display: none;'))]//tr[2]//td[contains(@class,'fare_header_basic')]//input[1]");
    By continueBtnOnFlightEle = By.xpath(".//a[@class='button lightbox-opener']");
    By dontWantToSaveEle = By.xpath(".//*[@id='VClubFareModal']//input[contains(@class, 'btnNoSave')]");
    By departingFlightVClubEle = By.xpath(".//*[@id='regular-fare-list-0']//*[@id='trip_0_passFare_0_vclub;']");
    By returningFlightVClubEle = By.xpath(".//*[@id='regular-fare-list-1']//*[@id='trip_1_passFare_1_vclub;']");
    By intialLoaderEle = By.xpath("//*[@id='system-loader']");
    By loaderEleActive = By.xpath("//*[@id='system-loader' and @style='display: block;']");
    By departingFlightListEle = By.xpath(".//div[@id='regular-fare-list-0']//*[contains(@id,'_regular')]");
    By returningFlightListEle = By.xpath(".//div[@id='regular-fare-list-1']//*[contains(@id,'_regular')]");
    By departingFlightListVClubEle = By.xpath(".//div[@id='regular-fare-list-0']//*[contains(@id,'_vclub')]");
    By returningFlightListVClubEle = By.xpath(".//div[@id='regular-fare-list-1']//*[contains(@id,'_vclub')]");

    Jexecutor jexecutor;
    DynamicWait dynamicWait;
    
    public FlightAvailabilityPage(RemoteWebDriver driver) {
        super(driver);
        jexecutor = new Jexecutor(driver);
        dynamicWait = new DynamicWait(driver);
    }


    public By getFlightLocator(String fareType,String flightCode) {
        String f = String.format("//*[contains(@data-faretype,'%s') and contains(@data-flighttype,'%s')]", fareType, flightCode);
        return By.xpath(f);
    }

    public void selectFlightsVclubMob(String journeyType) throws InterruptedException {
    	dynamicWait.checkPageLoaderInvisiblity();
        if(isObjectAvailable(flightPageEle)) {
        	log1.info("Selecting flight.");
            jexecutor.scrollDown();
            jClick(departFlightTabEle);
	        jClick(departingFlightVClubEle);
            dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, intialLoaderEle);
	        if (!journeyType.equalsIgnoreCase("oneway")) {
	            dynamicWait.checkPageLoaderInvisiblity();
                jClick(returnFlightTabEle);
	            jClick(returningFlightVClubEle);
                dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, loaderEleActive);
	        }
	        jexecutor.scrollToEnd();
	        jClick(continueBtnOnFlightEle);
        } else
            throw new RuntimeException("Flights are not available!");
    }
    
    public void selectFlightsMob(String journeyType) throws InterruptedException {
    	dynamicWait.checkPageLoaderInvisiblity();
        if(isObjectAvailable(flightPageEle)) {
        	log1.info("Selecting flight.");
            jClick(departFlightTabEle);
            jClick(departingFlightEle);
            dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, intialLoaderEle);
            if (!journeyType.equalsIgnoreCase("oneway")) {
                dynamicWait.checkPageLoaderInvisiblity();
                jClick(returnFlightTabEle);
                jClick(returningFlightEle);
                dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, loaderEleActive);
            }
            jClick(continueBtnOnFlightEle);
            if (isObjectAvailable(dontWantToSaveEle, 2))
            	jClick(dontWantToSaveEle);
            else
            	System.out.println("");
        } else
            throw new RuntimeException("Flights are not available!");
    }

    public void selectFlightsVclub(String journeyType) throws InterruptedException {
    	dynamicWait.checkPageLoaderInvisiblity();
        if(isObjectAvailable(flightPageEle)) {
        	log1.info("Selecting flight.");
        	elementList(departingFlightListVClubEle);
            dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, intialLoaderEle);
	        if (!journeyType.equalsIgnoreCase("oneway")) {
	        	dynamicWait.driverWait(DynamicWait.WaitConditions.CLICKABLE, returningFlightVClubEle);
	            elementList(returningFlightListVClubEle);
                dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, loaderEleActive);
	        }
            dynamicWait.checkPageLoaderInvisiblity();
	        jexecutor.scrollToEnd();
	        jClick(continueBtnOnFlightEle);
        } else
            throw new RuntimeException("Flights are not available!");
    }

    
    public void selectFlights(String journeyType) throws InterruptedException {
        //dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, intialLoaderEle);
        if(isObjectAvailable(flightPageEle)) {
        	log1.info("Selecting flight.");
        	elementList(departingFlightEle);
//        	getFlight(flightType);
            //getFlightWithFlightJourney(flightType);
            //dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, intialLoaderEle);
            if (!journeyType.equalsIgnoreCase("oneway")) {           	
            	elementList(returningFlightEle);
                //dynamicWait.driverWait(DynamicWait.WaitConditions.INVISIBLE, loaderEleActive);
            }
            dynamicWait.checkPageLoaderInvisiblity();
            jexecutor.scrollToEnd();
            jClick(continueBtnOnFlightEle);
            if (isObjectAvailable(dontWantToSaveEle, 2))
            	jClick(dontWantToSaveEle);
            else
            	System.out.println("");
        } else
            throw new RuntimeException("Flights are not available!");
    }
    
    public void getFlight(String flightType) {
        String f = String.format("//div[@id='tripIndexID_0']//*[@id='regular-fare-list-0']//*[contains(@data-faretype,'regular')]");
        List<WebElement> matchingFlights = driver.findElements(By.xpath(f));
        outerloop:
        for (WebElement element:matchingFlights) {
            String journeyType = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div/div/div[@class='flight-journey']")).getText();
            //int divCount = element.findElements(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator']")).size();
            switch (flightType.toLowerCase().trim()) {
            case "direct":
                if (journeyType.equalsIgnoreCase(flightType) || journeyType.equalsIgnoreCase("directo")) {
                    element.click();
                    break outerloop;
                }
                break;                
            case "connecting":
                if(journeyType.contains("stop") || journeyType.contains("Escala")) {
                    /*String connect = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div/div/div[@class='flight-journey']/../../following-sibling::div[1]")).getText();
                    String data[] = connect.split("/");
                    String stop1 = data[0];
                    String stop2 = data[1].substring(0,data[1].length()-1);*/

                    String stop1 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][2]")).getText();
                    String stop2 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][3]")).getText();
                    if(!stop1.trim().equalsIgnoreCase(stop2.trim())) {
                        element.click();
                        break outerloop;
                    }
                }
                break;
            case "thru":
                if(journeyType.contains("stop") || journeyType.contains("Escala")) {
                    /*String connect = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div/div/div[@class='flight-journey']/../../following-sibling::div[1]")).getText();
                    String data[] = connect.split("/");
                    String stop1 = data[0];
                    String stop2 = data[1].substring(0,data[1].length()-1);*/

                    String stop1 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][2]")).getText();
                    String stop2 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][3]")).getText();
                    if(stop1.trim().equalsIgnoreCase(stop2.trim())) {
                        element.click();
                        break outerloop;
                    }
                }
                break;
             default:
                 throw new NoSuchElementException( flightType +   " flight not available");
            }
        }
    }

    public void getFlightWithFlightJourney(String flightType) {
        String f = String.format("//div[@id='tripIndexID_0']//*[@id='regular-fare-list-0']//*[contains(@data-faretype,'regular')]");
        List<WebElement> matchingFlights = driver.findElements(By.xpath(f));
        outerloop:
        switch(flightType.toLowerCase().trim()) {
            case "direct":
                for (WebElement element:matchingFlights) {
                    String journeyType = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div/div/div[@class='flight-journey']")).getText();
                    if (journeyType.equalsIgnoreCase(flightType) || journeyType.equalsIgnoreCase("directo")) {
                        element.click();
                        break outerloop;
                    }
                }
                Assert.fail("Direct flight not found");
            case "connecting":
                for (WebElement element:matchingFlights) {
                    String journeyType = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div/div/div[@class='flight-journey']")).getText();
                    if(journeyType.contains("stop") || journeyType.contains("Escala")) {
                        String stop1 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][2]")).getText();
                        String stop2 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][3]")).getText();
                        if(!stop1.trim().equalsIgnoreCase(stop2.trim())) {
                            element.click();
                            break outerloop;
                        }
                    }
                }
                Assert.fail("Connecting flight not found");
            case "thru":
                for (WebElement element:matchingFlights) {
                    String journeyType = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div/div/div[@class='flight-journey']")).getText();
                    if(journeyType.contains("stop") || journeyType.contains("Escala")) {
                        String stop1 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][2]")).getText();
                        String stop2 = element.findElement(By.xpath("../parent::div/div[@class='flight-content']/div[2]/div/div[@class='operator'][3]")).getText();
                        if(stop1.trim().equalsIgnoreCase(stop2.trim())) {
                            element.click();
                            break outerloop;
                        }
                    }
                }
                Assert.fail("Thru flight not found");
        }
    }
}
